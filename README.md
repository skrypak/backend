# CMS

### Implemented features:

- Rest API
- Authorization (JWT Tokens)
- MySQL (TypeORM)
- Web Sockets (in progress...)
- API Documentation (Swagger)
- Unit Tests (in progress...)
- E2E Tests (in progress...)

## Installation

```bash
$ npm install
```



## Running the app
```bash
$ docker-compose up
```
or

```bash
# apply migrations

# production mode
$ npm run migrate:run

# development mode
$ npm run dev:migrate:run

# development
$ npm run start


# watch mode
$ npm run start:dev

# production mode
npm run start:prod
```

## Info
default user
 L: admin@mail.com
 P: 8eKBUc


## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
