require('dotenv').config({ path: './docker/environments' });

const {
  DB_HOST = 'localhost',
  DB_PORT = 3306,
  DB_USER = 'root',
  DB_PASSWORD = 'root',
  DB_NAME = 'company_name_cms'
} = process.env;

module.exports = [
  {
    "type": "mysql",
    "host": DB_HOST,
    "port": DB_PORT,
    "username": DB_USER,
    "password": DB_PASSWORD,
    "database": DB_NAME,
    "autoSchemaSync": true,
    "entities": [`./**/*.entity.ts`],
    "migrations": [
      "src/migration/*.ts"
    ],
    "cli": {
      "migrationsDir": "src/migration",
    }
  }
]
