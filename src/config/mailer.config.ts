import { MailerModuleOptions } from '@nest-modules/mailer';

const {
  MAIL_HOST,
  MAIL_PORT,
  MAIL_USER,
  MAIL_PASSWORD,
  MAIL_SEND_TO,
} = process.env;

export const MAILER_CONFIG: MailerModuleOptions = {
  transport: {
    host: MAIL_HOST,
    port: MAIL_PORT,
    secure: true,
    auth: {
      user: MAIL_USER,
      pass: MAIL_PASSWORD,
    },
  },
  defaults: {
    forceEmbeddedImages: true,
    from: `"nest-modules" <${MAIL_USER}>`,
  },
  templateDir: 'src/common/email-templates',
  templateOptions: {
    engine: 'handlebars',
  },
};