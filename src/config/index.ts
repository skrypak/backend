import { join } from 'path';

const FILE_STORAGE_PATH = join(__dirname, `../../static`);

const {
  DB_HOST,
  DB_PORT,
  DB_USER,
  DB_PASSWORD,
  DB_NAME,
  LOG_LEVEL,
  JWT_SECRET,
  JWT_EXPIRES,
  PORT,
  ACCESS_KEY_ID,
  SECRET_ACCESS_KEY,
  REGION,
  BUCKET,
  KEY,
  MAIL_HOST,
  MAIL_PORT,
  MAIL_USER,
  MAIL_PASSWORD,
} = process.env;
import { ORM_CONFIG } from './orm.config';
import { MAILER_CONFIG } from './mailer.config';

export {
  DB_HOST,
  DB_PORT,
  DB_USER,
  DB_PASSWORD,
  DB_NAME,
  FILE_STORAGE_PATH,
  LOG_LEVEL,
  ORM_CONFIG,
  JWT_SECRET,
  JWT_EXPIRES,
  PORT,
  ACCESS_KEY_ID,
  SECRET_ACCESS_KEY,
  REGION,
  BUCKET,
  KEY,
  MAILER_CONFIG,
  MAIL_HOST,
  MAIL_PORT,
  MAIL_USER,
  MAIL_PASSWORD,
};