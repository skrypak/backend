export class Dictionary<TValue> {
  [id: string]: TValue;
}