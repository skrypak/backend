export enum Role {
    USER = 'USER',
    ADMIN = 'ADMIN',
    PROJECT_MANAGER = 'PROJECT MANAGER',
    SELLER = 'SELLER',
  }