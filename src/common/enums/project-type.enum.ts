export enum ProjectType {
    INTERNAL = 'INTERNAL',
    COMMERCE = 'COMMERCE',
}
