export enum SocialType {
  FACEBOOK = 'Facebook',
  YOUTUBE = 'YouTube',
  INSTAGRAM = 'Instagram',
  TWITTER = 'Twitter',
  REDDIT = 'Reddit',
  PINTEREST = 'Pinterest',
  GOOGLE_PLUS = 'Google+',
  LINKEDIN = 'LinkedIn',
}
