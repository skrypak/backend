export enum IdpGoalStatus {
  IN_PROGRESS = 'IN_PROGRESS',
  ON_HOLD = 'ON_HOLD',
  IN_REVIEW = 'IN_REVIEW',
  COMPLETED = 'COMPLETED',
}
