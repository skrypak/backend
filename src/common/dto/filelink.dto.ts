import { IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class FileLinkDto {
  @IsString() @ApiModelProperty() readonly filename: string;
}
