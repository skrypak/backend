import { IsString, IsNumber, IsInstance } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UploadFileDto {
  @IsString() @ApiModelProperty() readonly mimetype: string;
  @IsString() @ApiModelProperty() readonly encoding: string;
  @IsInstance(Buffer) @ApiModelProperty() readonly buffer: Buffer;
  @IsNumber() @ApiModelProperty() readonly size: number;
  @IsString() @ApiModelProperty() readonly originalname: string;
}
