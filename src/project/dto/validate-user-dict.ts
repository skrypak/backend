import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';
import { UserProjectDto } from './project-user.dto';
import { Dictionary } from 'common/interfaces/dictionary';

@ValidatorConstraint({ name: 'userDictionaryValidator', async: false })
export class UserDictionaryValidator implements ValidatorConstraintInterface {

    validate(value: Dictionary<UserProjectDto[]>, args: ValidationArguments) {
      for (const key in value) {
        if (value.hasOwnProperty(key)) {
          if (!(value[key] instanceof Array)) return false;
          for (const el of value[key]) {
            if (typeof(el.id) === 'undefined') return false;
          }
        }
      }
      return true;
    }

    defaultMessage(args: ValidationArguments) {
        return 'Wrong structure of object';
    }
}