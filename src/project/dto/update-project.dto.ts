import { IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { CreateProjectDto } from './create-project.dto';

export class UpdateProjectDto extends CreateProjectDto {
  @IsNumber() @ApiModelProperty() readonly id: number;
}
