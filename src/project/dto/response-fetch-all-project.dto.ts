import { IsNumber, IsOptional, IsArray } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Project } from 'project/project.entity';

export class ResponseProjectDto {
  @IsNumber() @ApiModelProperty() readonly count: number;
  @IsNumber() @ApiModelProperty() readonly page: number;
  @IsNumber() @ApiModelProperty() readonly total: number;
  @IsArray() @ApiModelProperty() readonly data: Project[];
}
