import { IsString, IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UserProjectDto {
  @IsNumber() @ApiModelProperty() readonly id: number;
}
