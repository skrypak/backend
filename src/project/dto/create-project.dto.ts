import { IsString, IsOptional, IsNumber, IsDateString, IsEnum, IsArray, ValidateNested, Validate } from 'class-validator';
import { Payment } from 'common/enums/payment.enum';
import { CooperationType } from 'common/enums/project-cooperation-type.enum';
import { ProjectType } from 'common/enums/project-type.enum';
import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { UserProjectDto } from './project-user.dto';
import { CreateProjectToolsDto } from 'project-tools/dto/create-project-tools.dto';
import { UserDictionaryValidator } from './validate-user-dict';
import { Dictionary } from 'common/interfaces/dictionary';
import { ReferenceEntityDto } from './project-technology.dto';
import { ProjectUser } from 'project-user/project-user.entity';
import { UpdateProjectUserDto } from 'project-user/dto/update-project-user.dto';

export class CreateProjectDto {
  @IsString()
  @ApiModelProperty()
  readonly name: string;

  @IsNumber()
  @ApiModelProperty()
  readonly status: number;

  @IsOptional()
  @ApiModelProperty({
    type: UpdateProjectUserDto,
  })
  readonly usersRef?: UpdateProjectUserDto[];

  @IsArray()
  @IsOptional()
  @ValidateNested()
  @Type(() => UserProjectDto)
  @ApiModelProperty({isArray: true, type: ReferenceEntityDto})
  readonly technologies?: ReferenceEntityDto[];

  @IsEnum(ProjectType)
  @ApiModelProperty({ enum: ProjectType })
  readonly type: ProjectType;

  @IsNumber()
  @ApiModelProperty()
  readonly client: number;

  @IsNumber()
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly saleManager?: number;

  @IsEnum(Payment)
  @ApiModelProperty({ enum: Payment })
  readonly payment: Payment;

  @IsEnum(CooperationType)
  @ApiModelProperty({ enum: CooperationType })
  readonly cooperation: CooperationType;

  @IsString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly contactPerson?: string;

  @IsNumber()
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly estimate?: number;

  @IsDateString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly date_start_job?: Date;

  @IsDateString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly date_end_job?: Date;

  @IsString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly permissions?: string;

  @IsArray()
  @IsOptional()
  @ValidateNested()
  @Type(() => CreateProjectToolsDto)
  @ApiModelProperty()
  readonly tools?: CreateProjectToolsDto[];
}
