import { IsString, IsOptional, IsEnum } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { QueryOrder } from 'common/enums/query-order.enum';
import { Payment } from 'common/enums/payment.enum';
import { ProjectType } from 'common/enums/project-type.enum';
import { CooperationType } from 'common/enums/project-cooperation-type.enum';

export class FilterProjectDto {
  @IsString() @IsOptional() @ApiModelPropertyOptional() readonly technologies?: string;
  @IsString() @IsOptional() @ApiModelPropertyOptional() readonly status?: string;
  @IsEnum(ProjectType) @IsOptional() @ApiModelPropertyOptional() readonly type?: ProjectType;
  @IsEnum(Payment) @IsOptional() @ApiModelPropertyOptional() readonly payment?: Payment;
  @IsEnum(CooperationType) @IsOptional() @ApiModelPropertyOptional() readonly cooperation?: CooperationType;
  @IsString() @IsOptional() @ApiModelPropertyOptional() readonly page?: string;
  @IsString() @IsOptional() @ApiModelPropertyOptional() readonly count?: string;
  @IsString() @IsOptional() @ApiModelPropertyOptional() readonly orderBy?: string;
  @IsEnum(QueryOrder) @IsOptional() @ApiModelPropertyOptional() readonly orderType?: QueryOrder;
  @IsString() @IsOptional() @ApiModelPropertyOptional() readonly tools?: string;
  @IsString() @IsOptional() @ApiModelPropertyOptional() readonly search?: string;
}
