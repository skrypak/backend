import { IsString, IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateProjectImageDto {
  @IsString() @ApiModelProperty() readonly projectId: number;
  @IsString() @ApiModelProperty() readonly filename: string;
  }
