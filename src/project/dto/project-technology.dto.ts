import { IsNumber } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ReferenceEntityDto {
  @IsNumber() @ApiModelProperty() readonly id: number;
}
