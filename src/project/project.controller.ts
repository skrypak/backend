import {
  Controller,
  Post,
  Body,
  Put,
  Param,
  Get,
  Delete,
  UseGuards,
  HttpException,
  HttpStatus,
  Query,
  UseInterceptors,
  FileInterceptor,
  UploadedFile,
} from '@nestjs/common';
import { ApiOperation, ApiUseTags, ApiBearerAuth, ApiImplicitParam, ApiConsumes, ApiImplicitFile, ApiImplicitBody } from '@nestjs/swagger';
import { Project } from './project.entity';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { DeleteResult } from 'typeorm';
import { AuthGuard } from '@nestjs/passport';
import { ProjectService } from './project.service';
import { ResponseProjectDto } from './dto/response-fetch-all-project.dto';
import { UploadFileDto } from 'common/dto/uploadfile.dto';
import { FileLinkDto } from 'common/dto/filelink.dto';
import { FileService } from '../core/file.service';
import { FilterProjectDto } from './dto/filter-project.dto';

@Controller('project')
@ApiUseTags('project')
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
export class ProjectController {
  constructor(
    private readonly projectService: ProjectService,
    private readonly fileService: FileService) {
  }

  @Delete('/:id')
  @ApiOperation({ title: 'Delete project by id' })
  async remove(@Param() id: string | number): Promise<DeleteResult> {
    return this.projectService.removeById(id);
  }

  @Put('/:id')
  @ApiImplicitParam({name: 'id', required: true})
  @ApiOperation({ title: 'Update project by id' })
  async update(@Param('id') id: string | number, @Body() updateProjectDto: UpdateProjectDto): Promise<Project> {
    const project = await this.projectService.findById(updateProjectDto.id);
    if (!project) throw new HttpException(`Can\'t find project with id '${id}'`, HttpStatus.BAD_REQUEST);
    return this.projectService.update(updateProjectDto);
  }

  @Post()
  @ApiOperation({ title: 'Create new project' })
  async create(@Body() createProjectDto: CreateProjectDto): Promise<Project> {
    const project = await this.projectService.findByName(createProjectDto.name);
    if (project) throw new HttpException('Project with this name already exists', HttpStatus.BAD_REQUEST);
    return this.projectService.create(createProjectDto);
  }

  @Get('/all')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ title: 'Get all projects(not paginated)' })
  findAll(): Promise<Project[]> {
    return this.projectService.getAll();
  }

  @Get('/:id')
  @ApiOperation({ title: 'Get project by id' })
  async fetchById(@Param() id: string | number): Promise<Project> {
    return this.projectService.findById(id);
  }

  @Get()
  @ApiOperation({ title: 'Get all projects' })
  async fetchAll(@Query() query: FilterProjectDto): Promise<ResponseProjectDto> {
    return this.projectService.findAll(query);
  }

  @Post('/image')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @UseInterceptors(FileInterceptor('file'))
  @ApiOperation({ title: 'Upload project picture' })
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'file', required: true, description: 'Image file for project picture' })
  @ApiImplicitBody({ name: 'id', type: Number, required: true })
  async uploadFile(@UploadedFile() file: UploadFileDto, @Body('id') projectId: number): Promise<FileLinkDto> {
    if (!file.mimetype) throw new HttpException('Can\'t upload file with undefined format', HttpStatus.BAD_REQUEST);
    if (!projectId) throw new HttpException('ProjectId is undefined!', HttpStatus.BAD_REQUEST);

    const user = await this.projectService.findById(projectId);
    if (!user) throw new HttpException('Can\'t find project with this id', HttpStatus.BAD_REQUEST);

    const filename = await this.fileService.uploadFile(file);
    if (!filename)  throw new HttpException('Can\'t upload file on S3', HttpStatus.BAD_REQUEST);

    return this.projectService.updateProjectPhoto({ projectId, filename });
  }

}
