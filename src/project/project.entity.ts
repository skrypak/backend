import { Entity, Column, JoinColumn, OneToMany, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { ProjectStatus } from '../project-status/project-status.entity';
import { ProjectType } from '../common/enums/project-type.enum';
import { Payment } from '../common/enums/payment.enum';
import { CooperationType } from '../common/enums/project-cooperation-type.enum';
import { BaseEntity } from '../common/entity/base.entity';
import { User } from '../user/user.entity';
import { Technology } from '../technology/technology.entity';
import { Client } from '../client/client.entity';
import { ProjectTools } from '../project-tools/project-tools.entity';
import { ProjectUser } from '../project-user/project-user.entity';

@Entity()
export class Project extends BaseEntity {

  @Column({ length: 255, nullable: false, unique: true })
  name: string;

  @ManyToOne(type => ProjectStatus, status => status.projects)
  @JoinColumn()
  status: ProjectStatus;

  @Column('enum', { enum: ProjectType })
  type: ProjectType;

  @ManyToOne(type => Client, client => client.projects)
  @JoinColumn()
  client: Client;

  @ManyToMany(type => Technology, skill => skill.projects)
  @JoinTable({ name: 'project_has_technologies' })
  technologies: Technology[];

  @OneToMany(type => ProjectUser, projectUser => projectUser.project)
  usersRef: ProjectUser[];

  @ManyToOne(type => User, saleManager => saleManager.projectsForSale)
  @JoinColumn()
  saleManager: User;

  @Column('enum', { enum: Payment })
  payment: Payment;

  @Column('enum', { enum: CooperationType })
  cooperation: CooperationType;

  @Column({ nullable: true })
  contactPerson?: string;

  @Column({ nullable: true })
  estimate?: number;

  @Column({ type: Date, nullable: true })
  date_start_job?: Date;

  @Column({ type: Date, nullable: true })
  date_end_job?: Date;

  @Column({ nullable: true })
  permissions?: string;

  @Column({ length: 255, nullable: true})
  image: string;

  @OneToMany(type => ProjectTools, tools => tools.project)
  tools: ProjectTools[];

  constructor(partial: Partial<Project>) {
    super();
    Object.assign(this, partial);
  }
}
