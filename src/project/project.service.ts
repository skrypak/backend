import { Injectable } from '@nestjs/common';
import { Repository, DeleteResult, getManager, In } from 'typeorm';
import { Project } from './project.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { ProjectStatus } from 'project-status/project-status.entity';
import { User } from 'user/user.entity';
import { ResponseProjectDto } from 'project/dto/response-fetch-all-project.dto';
import { QueryOrder } from 'common/enums/query-order.enum';
import { FilterProjectDto } from './dto/filter-project.dto';
import { Technology } from '../technology/technology.entity';
import { Client } from 'client/client.entity';
import { ProjectTools } from 'project-tools/project-tools.entity';
import { FileLinkDto } from 'common/dto/filelink.dto';
import { UpdateProjectImageDto } from './dto/update-project-image.dto';
import { ProjectUser } from '../project-user/project-user.entity';

const relations = ['usersRef', 'usersRef.role', 'usersRef.user', 'technologies', 'status', 'saleManager', 'client', 'tools'];

@Injectable()
export class ProjectService {
  constructor(
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
  ) { }

  createUsersRef(usersRef, id: number) {
    return usersRef.map(el => ({
      ...el,
      role: { id: el.roleId },
      user: { id: el.userId },
      project: { id },
    }));
  }

  async create(createProjectDto: CreateProjectDto): Promise<Project> {
    const params = {
      ...createProjectDto,
      status: { id: createProjectDto.status } as ProjectStatus,
      saleManager: { id: createProjectDto.saleManager } as User,
      technologies: createProjectDto.technologies as Technology[],
      client: { id: createProjectDto.client } as Client,
    };
    let project;
    let projectUser;
    await getManager().transaction(async transactionalEntityManager => {
      project = await transactionalEntityManager.save(Project, params);

      const usersRef = this.createUsersRef(project.usersRef, project.id);

      projectUser = await transactionalEntityManager.save(ProjectUser, usersRef);

      if (createProjectDto.tools) {
        const tools = [];
        for (const tool of createProjectDto.tools) {
          const t = new ProjectTools({ ...tool, project } as ProjectTools);
          tools.push(t);
        }
        await transactionalEntityManager.save(ProjectTools, tools);
      }
    });
    return this.projectRepository.findOne(project.id, { relations });
  }

  async update(updateProjectDto: UpdateProjectDto): Promise<Project> {
    const usersRef = this.createUsersRef(updateProjectDto.usersRef, updateProjectDto.id);
    const project = {
      ...updateProjectDto,
      usersRef,
      status: { id: updateProjectDto.status } as ProjectStatus,
      saleManager: { id: updateProjectDto.saleManager } as User,
      technologies: updateProjectDto.technologies ? updateProjectDto.technologies.map((item) => new Technology(item)) : [],
      client: { id: updateProjectDto.client } as Client,
    };

    await getManager().transaction(async transactionalEntityManager => {
      await transactionalEntityManager.save(Project, project);

      const current = await transactionalEntityManager.find(ProjectUser, { where: { projectId: project.id as number } });

      const diff = current.filter(uP => !project.usersRef.find(_uP => uP.id === _uP.id));
      if (diff.length > 0) await transactionalEntityManager.delete(ProjectUser, { id: In(diff.map(d => d.id)) });

      await transactionalEntityManager.save(ProjectUser, usersRef);
    });

    return this.findById(updateProjectDto.id);
  }

  async findByName(name: string) {
    return this.projectRepository.findOne({ name }, { relations });
  }

  async findAll(query: FilterProjectDto): Promise<ResponseProjectDto> {
    const { count = 10, page = 1, orderType = QueryOrder.DESC, orderBy = 'name', search } = query;
    const searchPattern = `%${search}%`;

    const sqlQuery = getManager().createQueryBuilder().select('project').from(Project, 'project')
      .leftJoinAndSelect('project.usersRef', 'usersRef')
      .leftJoinAndSelect('usersRef.user', 'user')
      .leftJoinAndSelect('project.saleManager', 'user.saleManager')
      .leftJoinAndSelect('project.client', 'client.projects');

    const { technologies, status, type } = query;

    status ?
      sqlQuery.innerJoinAndSelect('project.status',
        'status.projects',
        'project.statusId IN (:...status)',
        { status: status.split(',') }) :
      sqlQuery.innerJoinAndSelect('project.status', 'status.projects');

    technologies ?
      sqlQuery.innerJoinAndSelect('project.technologies',
        'technology.projects',
        'technologyId IN (:...ids) ',
        { ids: technologies.split(',') }) :
      sqlQuery.leftJoinAndSelect('project.technologies', 'technology.projects');

    if (type) sqlQuery.where('type IN (:...type)', { type: type.split(',') });

    if (search) {
      sqlQuery.where('project.name LIKE :name', { name: searchPattern });
    }

    const order = {};
    order[`project.${orderBy}`] = orderType || QueryOrder.DESC;
    const allProjects = await sqlQuery.getCount();
    const projects = await sqlQuery
      .leftJoinAndSelect('project.tools', 'project_tools')
      .take(count as number)
      .skip(page === 1 ? 0 : count as number * (parseInt(page, 10) as number - 1))
      .orderBy(order)
      .getMany();

    return await Promise.resolve({
      count: count as number,
      page: page as number,
      total: allProjects as number,
      data: projects as Project[],
    });
  }

  async findById(id: string | number): Promise<Project> {
    return this.projectRepository.findOne(id, { relations });
  }

  async removeById(id: number | string): Promise<DeleteResult> {
    return this.projectRepository.delete(id);
  }

  async updateProjectPhoto(updateProjectImageDto: UpdateProjectImageDto): Promise<FileLinkDto> {
    await this.projectRepository.update(updateProjectImageDto.projectId, { image: updateProjectImageDto.filename });
    return { filename: updateProjectImageDto.filename };
  }

  async getAll(): Promise<Project[]> {
    return this.projectRepository.find();
  }
}
