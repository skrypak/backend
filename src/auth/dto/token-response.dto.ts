import { ApiModelProperty } from '@nestjs/swagger';

const TOKEN_EXAMPLE: string = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWlu'
  + 'QG1haWwuY29tIiwiaWQiOjEsImlhdCI6MTU0NDUzMjIyMCwiZXhwIjoxNTQ0NjE4NjIwfQ.j_vbl0GTkvxAt59418GuzNkFFJDBsQwIWSXXyVdBfg8';

export class TokenResponseDto {

  @ApiModelProperty({ example: '1d' })
  readonly expires_in: string;

  @ApiModelProperty({ example: TOKEN_EXAMPLE })
  readonly access_token: string;
}