import { Controller, Post, Body, HttpException, HttpStatus, UseGuards, Req } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiBearerAuth, ApiResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

import { AuthService } from './auth.service';
import { UserService } from '../user/user.service';
import { LoginDto } from './dto/login.dto';
import { PasswordService } from '../core/password.service';
import { TokenResponse } from './interfaces/token-response.interface';
import { TokenResponseDto } from './dto/token-response.dto';

@ApiUseTags('auth')
@Controller('auth')
export class AuthController {

  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly passwordService: PasswordService,
  ) { }

  @Post('login')
  @ApiOperation({ title: 'Login user' })
  @ApiResponse({ status: 200, type: TokenResponseDto })
  async login(@Body() { email, password }: LoginDto): Promise<TokenResponse> {
    const user = await this.userService.findByEmailAuth(email);
    if (!user) throw new HttpException('User was not found', HttpStatus.NOT_FOUND);
    const isValid = await this.passwordService.comparePassword(password, user.password_hash);
    if (!isValid) throw new HttpException('Password is incorrect', HttpStatus.UNAUTHORIZED);

    return this.authService.createToken(email, user.id);
  }

  @Post('refresh-token')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ title: 'Refresh Token' })
  async refreshToken(@Req() req): Promise<TokenResponse> {
    const { email, id } = req.user;
    return this.authService.createToken(email, id);
  }
}
