import { EntityManager, In } from 'typeorm';
import { RelatedModel } from 'common/interfaces/related-model';
import { Injectable } from '@nestjs/common';

@Injectable()
export class EntityService {
    /**
     * Find existing entities and create not existing
     * @param {EntityManager} entry
     * @param {T[]} entityData
     * @param {T[]} TYPE
     */
    async findOrAddRelatedModel<T extends RelatedModel>(entry: EntityManager, technologies: T[], TYPE: any): Promise<T[]> {
      if (!technologies.length) return technologies;
      const names = technologies.map(t => t.name);
      const existing: T[] = await entry.find<T>(TYPE, {where: {name: In(names) }});

      // if all technologies is found we not need create anything
      if (existing.length === technologies.length) return existing;

      const existingNames = existing.map(t => t.name);

      // filter only not existing technologies and add them
      let notExist = technologies.filter(t => !existingNames.some(e => e === t.name));
      const result = await entry
          .createQueryBuilder()
          .insert()
          .into(TYPE)
          .values(notExist)
          .execute();

      // add id of added technologies to list of not existing
      notExist = notExist.map((t, i) => ({name: t.name, id: result.identifiers[i].id} as T)) ;

      return [...existing, ...notExist];
    }
}