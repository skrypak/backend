import { Module } from '@nestjs/common';
import { PasswordService } from './password.service';
import { DateTimeService } from './date-time.service';
import { FileService } from './file.service';
import { EntityService } from './entity.service';
import { EmailService } from './email.service';

@Module({
  providers: [PasswordService, DateTimeService, FileService, EntityService, EmailService],
  exports: [PasswordService, DateTimeService, FileService, EntityService, EmailService],
  controllers: [],
})
export class CoreModule { }
