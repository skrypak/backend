import { Injectable } from '@nestjs/common';
import * as moment from 'moment';

@Injectable()
export class DateTimeService {

  diffInMonths(start: string | Date, end: string | Date): number {
    return moment(end).diff(moment(start), 'months');
  }

  diffInDays(start: string | Date, end: string | Date | moment.Moment): number {
    return moment(end).diff(moment(start), 'days');
  }

  checkDuration(start, end, minDuration, maxDuration): void {
    const diff = this.diffInMonths(start, end);

    if (diff < minDuration) {
      throw new Error(`The difference between startdate and enddate should be at lest ${minDuration} month`);
    }
    if (diff > maxDuration) {
      throw new Error(`The difference between startdate and enddate should not be more than ${maxDuration} months`);
    }
  }

}
