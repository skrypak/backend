import * as fs from 'fs';
import uuid = require('uuid');
import { FILE_STORAGE_PATH, ACCESS_KEY_ID, SECRET_ACCESS_KEY, REGION, BUCKET, KEY } from 'config';
import * as S3 from 'aws-sdk/clients/s3';
import { UploadFileDto } from 'common/dto/uploadfile.dto';
import { Injectable } from '@nestjs/common';

@Injectable()
export class FileService {
  /**
   * remove file by file name from folder `static`
   * @param {string} filename
   */
  async deleteByName(filename: string) {
    await fs.promises.unlink(`${FILE_STORAGE_PATH}/${filename}`);
  }

  uploadFile(file: UploadFileDto): Promise<string> {
    const format = file.mimetype.replace(/[a-z]*\//, '');
    const filename = `${uuid.v4()}.${format}`;

    return new Promise((resolve, reject) => {
      const bucket: S3 = new S3(
        {
          accessKeyId: ACCESS_KEY_ID,
          secretAccessKey: SECRET_ACCESS_KEY,
          region: REGION,
        },
      );
      const params = {
        Bucket: BUCKET,
        Key: KEY + filename,
        Body: file.buffer,
      };

      bucket.upload(params, (err, data) => {
        if (err) {
          return reject(err);
        }
        resolve(data.Location as string);
      });
    });
  }
}