
import { Injectable, Inject } from '@nestjs/common';

@Injectable()
export class EmailService {
    constructor(
        @Inject('MailerProvider') private readonly mailerProvider,
    ) { }

    async sendResetPassEmail(to: string, from: string, subject: string, href: string, token: string, username: string) {
        username = username || 'anonymity lover';
        return await this.mailerProvider.sendMail({
            to,
            from,
            subject,
            template: 'reset-password',
            context: {  // Data to be sent to template engine.
                href,
                token,
                username,
            },
        });
    }
}
