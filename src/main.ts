import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { HTTP_SERVER_REF } from '@nestjs/core';

import { AppModule } from './app.module';
import { ReqeustLoggerInterceptor } from './common/interceptors';
import { ValidationPipe } from './common/pipes/validation.pipe';
import { FILE_STORAGE_PATH } from './config';
import { AllExceptionsFilter } from 'common/filters';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  app.useGlobalInterceptors(new ReqeustLoggerInterceptor());
  app.useGlobalPipes(new ValidationPipe());
  app.useStaticAssets(FILE_STORAGE_PATH);
  app.setGlobalPrefix('/api/v1');
  const httpRef = app.get(HTTP_SERVER_REF);
  app.useGlobalFilters(new AllExceptionsFilter(httpRef));

  const options = new DocumentBuilder()
    .setBasePath('/api/v1')
    .setTitle('Company Management Service REST API Documentation')
    .setDescription('The API description')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('/api-docs', app, document);

  await app.listen(process.env.PORT);
}
bootstrap();
