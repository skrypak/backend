import { MigrationInterface, QueryRunner } from 'typeorm';

export class PaymentTypes1544091083961 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `project` MODIFY `payment` enum ('FIXED_PRICE', 'TIME_AND_MATERIAL', 'HOURLY') NOT NULL"
    );
    await queryRunner.query("UPDATE `project` SET `payment` = 'FIXED_PRICE' WHERE `payment` = 'Fixed Price'");
    await queryRunner.query("UPDATE `project` SET `payment` = 'TIME_AND_MATERIAL' WHERE `payment` = 'Time & Materials'");
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      "ALTER TABLE `project` MODIFY `payment` enum ('FIXED_PRICE', 'TIME_AND_MATERIAL', 'HOURLY') NOT NULL"
    );
    await queryRunner.query("UPDATE `project` SET `payment` = 'FIXED_PRICE' WHERE `payment` = 'Fixed Price'");
    await queryRunner.query("UPDATE `project` SET `payment` = 'TIME_AND_MATERIAL' WHERE `payment` = 'Time & Materials'");
  }
}
