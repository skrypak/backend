import {MigrationInterface, QueryRunner} from "typeorm";

export class Init1539236511569 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `department` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, UNIQUE INDEX `IDX_471da4b90e96c1ebe0af221e07` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `client_status` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, UNIQUE INDEX `IDX_79c8e104b57804eff8e0a05611` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `project_status` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, UNIQUE INDEX `IDX_9d0a4ed696b85b68a07da85ee8` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `technology_type` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, UNIQUE INDEX `IDX_dd449dc1d801705d0488303c31` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user_technology` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `level` int NOT NULL, `userId` int NOT NULL, `technologyId` int NOT NULL, PRIMARY KEY (`id`, `userId`, `technologyId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `technology` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, `technologyTypeId` int NULL, UNIQUE INDEX `IDX_0e93116dd895bf20badb82d3ed` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `project_tools` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, `url` varchar(255) NULL, `projectId` int NOT NULL, `type` enum ('DEVELOPMENT', 'COMUNICATION', 'SERVICE') NULL, `access` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `project` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, `type` enum ('Design', 'Development') NOT NULL, `payment` enum ('Fixed Price', 'Time & Materials') NOT NULL, `contactPerson` varchar(255) NULL, `estimate` int NULL, `date_start_job` datetime NULL, `date_end_job` datetime NULL, `permissions` varchar(255) NULL, `statusId` int NULL, `clientId` int NULL, `saleManagerId` int NULL, UNIQUE INDEX `IDX_dedfea394088ed136ddadeee89` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `client` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `first_name` varchar(255) NULL, `last_name` varchar(255) NULL, `logo` varchar(255) NULL, `email` varchar(255) NOT NULL, `phone` varchar(45) NULL, `qualities` varchar(400) NULL, `contactPerson` varchar(255) NULL, `timezone` varchar(255) NOT NULL, `country` varchar(255) NOT NULL, `deletedAt` datetime NULL, `statusId` int NULL, `saleManager_id` int NULL, UNIQUE INDEX `IDX_6436cc6b79593760b9ef921ef1` (`email`), UNIQUE INDEX `IDX_368ca99acdbd5502fc08b3f779` (`phone`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `social` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `url` varchar(255) NOT NULL, `type` enum ('Facebook', 'YouTube', 'Instagram', 'Twitter', 'Reddit', 'Pinterest', 'Google+', 'LinkedIn') NULL, `userId` int NULL, `clientId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `positions` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, UNIQUE INDEX `IDX_5c70dc5aa01e351730e4ffc929` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `password_hash` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `first_name` varchar(255) NULL, `last_name` varchar(255) NULL, `phone` varchar(45) NULL, `photo` varchar(255) NULL, `gender` enum ('MALE', 'FEMALE') NULL, `role` enum ('USER', 'ADMIN', 'PROJECT MANAGER', 'SELLER') NULL, `experience` int NULL, `qualities` text NULL, `ssh_key` text NULL, `date_start_job` datetime NULL, `date_end_job` datetime NULL, `cityId` int NULL, `departmentId` int NULL, `positionId` int NULL, UNIQUE INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`), UNIQUE INDEX `IDX_8e1f623798118e629b46a9e629` (`phone`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `city` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, UNIQUE INDEX `IDX_f8c0858628830a35f19efdc0ec` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `project_has_technologies` (`projectId` int NOT NULL, `technologyId` int NOT NULL, PRIMARY KEY (`projectId`, `technologyId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `project_has_pm` (`projectId` int NOT NULL, `userId` int NOT NULL, PRIMARY KEY (`projectId`, `userId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `user_technology` ADD CONSTRAINT `FK_3736dabf0d00610a19f44afb343` FOREIGN KEY (`userId`) REFERENCES `user`(`id`)");
        await queryRunner.query("ALTER TABLE `user_technology` ADD CONSTRAINT `FK_39b1934d1b5577dd09dfde45a65` FOREIGN KEY (`technologyId`) REFERENCES `technology`(`id`)");
        await queryRunner.query("ALTER TABLE `technology` ADD CONSTRAINT `FK_8891c861ff2409a2dfdc44fa7ee` FOREIGN KEY (`technologyTypeId`) REFERENCES `technology_type`(`id`)");
        await queryRunner.query("ALTER TABLE `project_tools` ADD CONSTRAINT `FK_efbd0a3c8aa788e4f2e32d88d23` FOREIGN KEY (`projectId`) REFERENCES `project`(`id`)");
        await queryRunner.query("ALTER TABLE `project` ADD CONSTRAINT `FK_b6d55aff9b16e061712260da686` FOREIGN KEY (`statusId`) REFERENCES `project_status`(`id`)");
        await queryRunner.query("ALTER TABLE `project` ADD CONSTRAINT `FK_816f608a9acf4a4314c9e1e9c66` FOREIGN KEY (`clientId`) REFERENCES `client`(`id`)");
        await queryRunner.query("ALTER TABLE `project` ADD CONSTRAINT `FK_3858933777e8438614747f00636` FOREIGN KEY (`saleManagerId`) REFERENCES `user`(`id`)");
        await queryRunner.query("ALTER TABLE `client` ADD CONSTRAINT `FK_0ce939bf6640541190453d3693f` FOREIGN KEY (`statusId`) REFERENCES `client_status`(`id`)");
        await queryRunner.query("ALTER TABLE `client` ADD CONSTRAINT `FK_4ada42ecf80ba334bf9fc27c0d1` FOREIGN KEY (`saleManager_id`) REFERENCES `user`(`id`)");
        await queryRunner.query("ALTER TABLE `social` ADD CONSTRAINT `FK_4cda297c26dea7a3b8d08b9ba18` FOREIGN KEY (`userId`) REFERENCES `user`(`id`)");
        await queryRunner.query("ALTER TABLE `social` ADD CONSTRAINT `FK_a4a74dd9d0e301053aaf969e7c8` FOREIGN KEY (`clientId`) REFERENCES `client`(`id`)");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_beb5846554bec348f6baf449e83` FOREIGN KEY (`cityId`) REFERENCES `city`(`id`)");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_3d6915a33798152a079997cad28` FOREIGN KEY (`departmentId`) REFERENCES `department`(`id`)");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_93af21ecba4fa43c4c63d2456cd` FOREIGN KEY (`positionId`) REFERENCES `positions`(`id`)");
        await queryRunner.query("ALTER TABLE `project_has_technologies` ADD CONSTRAINT `FK_1e72e5ab6cf3e51f84413557626` FOREIGN KEY (`projectId`) REFERENCES `project`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `project_has_technologies` ADD CONSTRAINT `FK_64bcd50450961fbb4527bf6ba8d` FOREIGN KEY (`technologyId`) REFERENCES `technology`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `project_has_pm` ADD CONSTRAINT `FK_41a878c947ade58920e85127e00` FOREIGN KEY (`projectId`) REFERENCES `project`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `project_has_pm` ADD CONSTRAINT `FK_32eb3569004d9543e2c63dbe128` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `project_has_pm` DROP FOREIGN KEY `FK_32eb3569004d9543e2c63dbe128`");
        await queryRunner.query("ALTER TABLE `project_has_pm` DROP FOREIGN KEY `FK_41a878c947ade58920e85127e00`");
        await queryRunner.query("ALTER TABLE `project_has_technologies` DROP FOREIGN KEY `FK_64bcd50450961fbb4527bf6ba8d`");
        await queryRunner.query("ALTER TABLE `project_has_technologies` DROP FOREIGN KEY `FK_1e72e5ab6cf3e51f84413557626`");
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_93af21ecba4fa43c4c63d2456cd`");
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_3d6915a33798152a079997cad28`");
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_beb5846554bec348f6baf449e83`");
        await queryRunner.query("ALTER TABLE `social` DROP FOREIGN KEY `FK_a4a74dd9d0e301053aaf969e7c8`");
        await queryRunner.query("ALTER TABLE `social` DROP FOREIGN KEY `FK_4cda297c26dea7a3b8d08b9ba18`");
        await queryRunner.query("ALTER TABLE `client` DROP FOREIGN KEY `FK_4ada42ecf80ba334bf9fc27c0d1`");
        await queryRunner.query("ALTER TABLE `client` DROP FOREIGN KEY `FK_0ce939bf6640541190453d3693f`");
        await queryRunner.query("ALTER TABLE `project` DROP FOREIGN KEY `FK_3858933777e8438614747f00636`");
        await queryRunner.query("ALTER TABLE `project` DROP FOREIGN KEY `FK_816f608a9acf4a4314c9e1e9c66`");
        await queryRunner.query("ALTER TABLE `project` DROP FOREIGN KEY `FK_b6d55aff9b16e061712260da686`");
        await queryRunner.query("ALTER TABLE `project_tools` DROP FOREIGN KEY `FK_efbd0a3c8aa788e4f2e32d88d23`");
        await queryRunner.query("ALTER TABLE `technology` DROP FOREIGN KEY `FK_8891c861ff2409a2dfdc44fa7ee`");
        await queryRunner.query("ALTER TABLE `user_technology` DROP FOREIGN KEY `FK_39b1934d1b5577dd09dfde45a65`");
        await queryRunner.query("ALTER TABLE `user_technology` DROP FOREIGN KEY `FK_3736dabf0d00610a19f44afb343`");
        await queryRunner.query("DROP TABLE `project_has_pm`");
        await queryRunner.query("DROP TABLE `project_has_technologies`");
        await queryRunner.query("DROP INDEX `IDX_f8c0858628830a35f19efdc0ec` ON `city`");
        await queryRunner.query("DROP TABLE `city`");
        await queryRunner.query("DROP INDEX `IDX_8e1f623798118e629b46a9e629` ON `user`");
        await queryRunner.query("DROP INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` ON `user`");
        await queryRunner.query("DROP TABLE `user`");
        await queryRunner.query("DROP INDEX `IDX_5c70dc5aa01e351730e4ffc929` ON `positions`");
        await queryRunner.query("DROP TABLE `positions`");
        await queryRunner.query("DROP TABLE `social`");
        await queryRunner.query("DROP INDEX `IDX_368ca99acdbd5502fc08b3f779` ON `client`");
        await queryRunner.query("DROP INDEX `IDX_6436cc6b79593760b9ef921ef1` ON `client`");
        await queryRunner.query("DROP TABLE `client`");
        await queryRunner.query("DROP INDEX `IDX_dedfea394088ed136ddadeee89` ON `project`");
        await queryRunner.query("DROP TABLE `project`");
        await queryRunner.query("DROP TABLE `project_tools`");
        await queryRunner.query("DROP INDEX `IDX_0e93116dd895bf20badb82d3ed` ON `technology`");
        await queryRunner.query("DROP TABLE `technology`");
        await queryRunner.query("DROP TABLE `user_technology`");
        await queryRunner.query("DROP INDEX `IDX_dd449dc1d801705d0488303c31` ON `technology_type`");
        await queryRunner.query("DROP TABLE `technology_type`");
        await queryRunner.query("DROP INDEX `IDX_9d0a4ed696b85b68a07da85ee8` ON `project_status`");
        await queryRunner.query("DROP TABLE `project_status`");
        await queryRunner.query("DROP INDEX `IDX_79c8e104b57804eff8e0a05611` ON `client_status`");
        await queryRunner.query("DROP TABLE `client_status`");
        await queryRunner.query("DROP INDEX `IDX_471da4b90e96c1ebe0af221e07` ON `department`");
        await queryRunner.query("DROP TABLE `department`");
    }

}
