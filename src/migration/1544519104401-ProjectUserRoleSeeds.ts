import { MigrationInterface, QueryRunner } from "typeorm";

export class ProjectUserRoleSeeds1544519104401 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query("INSERT INTO `project_user_role` (name) VALUES ('Backend developer')");
    await queryRunner.query("INSERT INTO `project_user_role` (name) VALUES ('Frontend developer')");
    await queryRunner.query("INSERT INTO `project_user_role` (name) VALUES ('FullStack developer')");
    await queryRunner.query("INSERT INTO `project_user_role` (name) VALUES ('Project Manager')");
    await queryRunner.query("INSERT INTO `project_user_role` (name) VALUES ('Sale Manager')");
    await queryRunner.query("INSERT INTO `project_user_role` (name) VALUES ('QA')");
    await queryRunner.query("INSERT INTO `project_user_role` (name) VALUES ('DevOps')");
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query("Delete from `project_user_role`");
  }

}
