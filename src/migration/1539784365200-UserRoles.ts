import { MigrationInterface, QueryRunner } from "typeorm";

export class UserRoles1539784365200 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user` CHANGE `role` `roleId` enum ('USER', 'ADMIN', 'PROJECT MANAGER', 'SELLER') NULL");
        await queryRunner.query("CREATE TABLE `user_role` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(150) NOT NULL, UNIQUE INDEX `IDX_31f96f2013b7ac833d7682bf02` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `roleId`");
        await queryRunner.query("ALTER TABLE `user` ADD `roleId` int NOT NULL");
        await queryRunner.query("Insert into user_role (name) values ('ROOT'), ('ADMIN'), ('USER'), ('PROJECT_MANAGER')");
        await queryRunner.query("Update user set roleId=(Select id from user_role where name='ROOT') where email='admin@mail.com'");
        await queryRunner.query("Update user set roleId=(Select id from user_role where name='USER') where email<>'admin@mail.com'");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_c28e52f758e7bbc53828db92194` FOREIGN KEY (`roleId`) REFERENCES `user_role`(`id`)");
  }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_c28e52f758e7bbc53828db92194`");
        await queryRunner.query("ALTER TABLE `user` DROP COLUMN `roleId`");
        await queryRunner.query("ALTER TABLE `user` ADD `roleId` enum ('USER', 'ADMIN', 'PROJECT MANAGER', 'SELLER') NULL");
        await queryRunner.query("DROP INDEX `IDX_31f96f2013b7ac833d7682bf02` ON `user_role`");
        await queryRunner.query("DROP TABLE `user_role`");
        await queryRunner.query("ALTER TABLE `user` CHANGE `roleId` `role` enum ('USER', 'ADMIN', 'PROJECT MANAGER', 'SELLER') NULL");
        await queryRunner.query("Update user set role='ADMIN' where email='admin@mail.com'");
        await queryRunner.query("Update user set role='USER' where email<>'admin@mail.com'");
    }
}
