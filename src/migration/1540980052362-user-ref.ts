import {MigrationInterface, QueryRunner} from "typeorm";

export class userRef1540980052362 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE project_has_pm RENAME project_has_user;");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query("ALTER TABLE project_has_user RENAME project_has_pm;");
    }

}
