import {MigrationInterface, QueryRunner} from "typeorm";

export class idp1541685652967 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `idp` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `start` datetime NULL, `end` datetime NULL, `type` enum ('IN_PROGRESS', 'DRAFT', 'IN_REVIEW', 'FINAL_REVIEW', 'COMPLETED') NOT NULL, `userId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_start_job` `date_start_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_end_job` `date_end_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `client` CHANGE `deletedAt` `deletedAt` datetime NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_start_job` `date_start_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_end_job` `date_end_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `idp` ADD CONSTRAINT `FK_4cfc06835de51615af8d847fd12` FOREIGN KEY (`userId`) REFERENCES `user`(`id`)");
        await queryRunner.query("ALTER TABLE `project_has_user` ADD CONSTRAINT `FK_49e28c1516ecd2c63e14559d1cd` FOREIGN KEY (`projectId`) REFERENCES `project`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `project_has_user` ADD CONSTRAINT `FK_77193e238909e543c32519d5f83` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `project_has_user` DROP FOREIGN KEY `FK_77193e238909e543c32519d5f83`");
        await queryRunner.query("ALTER TABLE `project_has_user` DROP FOREIGN KEY `FK_49e28c1516ecd2c63e14559d1cd`");
        await queryRunner.query("ALTER TABLE `idp` DROP FOREIGN KEY `FK_4cfc06835de51615af8d847fd12`");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_end_job` `date_end_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_start_job` `date_start_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `client` CHANGE `deletedAt` `deletedAt` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_end_job` `date_end_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_start_job` `date_start_job` datetime(0) NULL");
        await queryRunner.query("DROP TABLE `idp`");
    }

}
