import { MigrationInterface, QueryRunner } from "typeorm";

export class Common1539236511570 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
       await queryRunner.query("Insert into city (name) values ('Zaporizhia'), ('Dnipro')");
       await queryRunner.query("Insert into department (name) values ('HRs'), ('Managers'), ('Sales'), ('Developers'), ('Office'), ('Officers')");
       await queryRunner.query("Insert into positions (name) values ('Recruter'), ('Project Manager'), ('Sales Manager'), ('Developer'), ('Office Manager'), ('CBDO'), ('CTO'), ('COO'), ('CHRO'), ('CEO')");
       await queryRunner.query("Insert into technology_type (name) values ('backend'), ('frontend')");
       await queryRunner.query("Insert into technology (name, technologyTypeId) values ('node.js', (select id from technology_type where name='backend')), ('react.js', (select id from technology_type where name='frontend'))");
       await queryRunner.query("Insert into project_status (name) values ('open'), ('to do'), ('canceled'), ('work'), ('on hold'), ('negotiation'), ('challenge'), ('refused'), ('done')");
       await queryRunner.query("Insert into client_status (name) values ('status1'), ('status2')");
       
       
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query("Delete from city where name='Zaporizhia' or name='Dnipro'");     
      await queryRunner.query("Delete from department where name='HRs' or name='Managers' or name='Sales' or name='Developers' or name='Office' or name='Officers'");       
      await queryRunner.query("Delete from positions where name='Recruter' or name='Project Manager' or name='Sales Manager' or name='Developer' or name='Office Manager' or name='CBDO' or name='CTO' or name='COO' or name='CHRO' or name='CEO'");             
      await queryRunner.query("Delete from technology where name='node.js' or name='react.js'");   
      await queryRunner.query("Delete from technology_type where name='backend' or name='frontend'"); 
      await queryRunner.query("Delete from project_status where name='open' or name='to do' or name='canceled' or name='work' or name='on hold' or name='negotiation' or name='challenge' or name='refused' or name='done'");     
      await queryRunner.query("Delete from client_status where name='status1' or name='status2'");     
    }

}
