import {MigrationInterface, QueryRunner} from 'typeorm';

export class newUserTech1544435408986 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('CREATE TABLE `user_has_technology` (`userId` int NOT NULL, `technologyId` int NOT NULL, PRIMARY KEY (`userId`, `technologyId`)) ENGINE=InnoDB');
        await queryRunner.query('ALTER TABLE `user_has_technology` ADD CONSTRAINT `FK_5739f5ffd6f3dfae5433c388d02` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE');
        await queryRunner.query('ALTER TABLE `user_has_technology` ADD CONSTRAINT `FK_9e2fd5252396d43c4f14c71b54d` FOREIGN KEY (`technologyId`) REFERENCES `technology`(`id`) ON DELETE CASCADE');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `user_has_technology` DROP FOREIGN KEY `FK_9e2fd5252396d43c4f14c71b54d`');
        await queryRunner.query('ALTER TABLE `user_has_technology` DROP FOREIGN KEY `FK_5739f5ffd6f3dfae5433c388d02`');
        await queryRunner.query('DROP TABLE `user_has_technology`');
    }

}
