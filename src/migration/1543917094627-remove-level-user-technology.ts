import {MigrationInterface, QueryRunner} from "typeorm";

export class removeLevelUserTechnology1543917094627 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user_technology` DROP COLUMN `level`");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user_technology` ADD `level` int NOT NULL");
    }
}
