export const countryArr: Array<{name: string, phoneCode: string}> = [
    {
        name: 'Afghanistan',
        phoneCode: '93',
    },
    {
        name: 'Åland Islands',
        phoneCode: '358',
    },
    {
        name: 'Albania',
        phoneCode: '355',
    },
    {
        name: 'Algeria',
        phoneCode: '213',
    },
    {
        name: 'American Samoa',
        phoneCode: '1684',
    },
    {
        name: 'Andorra',
        phoneCode: '376',
    },
    {
        name: 'Angola',
        phoneCode: '244',
    },
    {
        name: 'Anguilla',
        phoneCode: '1264',
    },
    {
        name: 'Antarctica',
        phoneCode: '672',
    },
    {
        name: 'Antigua and Barbuda',
        phoneCode: '1268',
    },
    {
        name: 'Argentina',
        phoneCode: '54',
    },
    {
        name: 'Armenia',
        phoneCode: '374',
    },
    {
        name: 'Aruba',
        phoneCode: '297',
    },
    {
        name: 'Australia',
        phoneCode: '61',
    },
    {
        name: 'Austria',
        phoneCode: '43',
    },
    {
        name: 'Azerbaijan',
        phoneCode: '994',
    },
    {
        name: 'Bahamas',
        phoneCode: '1242',
    },
    {
        name: 'Bahrain',
        phoneCode: '973',
    },
    {
        name: 'Bangladesh',
        phoneCode: '880',
    },
    {
        name: 'Barbados',
        phoneCode: '1246',
    },
    {
        name: 'Belarus',
        phoneCode: '375',
    },
    {
        name: 'Belgium',
        phoneCode: '32',
    },
    {
        name: 'Belize',
        phoneCode: '501',
    },
    {
        name: 'Benin',
        phoneCode: '229',
    },
    {
        name: 'Bermuda',
        phoneCode: '1441',
    },
    {
        name: 'Bhutan',
        phoneCode: '975',
    },
    {
        name: 'Bolivia (Plurinational State of)',
        phoneCode: '591',
    },
    {
        name: 'Bonaire, Sint Eustatius and Saba',
        phoneCode: '5997',
    },
    {
        name: 'Bosnia and Herzegovina',
        phoneCode: '387',
    },
    {
        name: 'Botswana',
        phoneCode: '267',
    },
    {
        name: 'Bouvet Island',
        phoneCode: '',
    },
    {
        name: 'Brazil',
        phoneCode: '55',
    },
    {
        name: 'British Indian Ocean Territory',
        phoneCode: '246',
    },
    {
        name: 'United States Minor Outlying Islands',
        phoneCode: '',
    },
    {
        name: 'Virgin Islands (British)',
        phoneCode: '1284',
    },
    {
        name: 'Virgin Islands (U.S.)',
        phoneCode: '1340',
    },
    {
        name: 'Brunei Darussalam',
        phoneCode: '673',
    },
    {
        name: 'Bulgaria',
        phoneCode: '359',
    },
    {
        name: 'Burkina Faso',
        phoneCode: '226',
    },
    {
        name: 'Burundi',
        phoneCode: '257',
    },
    {
        name: 'Cambodia',
        phoneCode: '855',
    },
    {
        name: 'Cameroon',
        phoneCode: '237',
    },
    {
        name: 'Canada',
        phoneCode: '1',
    },
    {
        name: 'Cabo Verde',
        phoneCode: '238',
    },
    {
        name: 'Cayman Islands',
        phoneCode: '1345',
    },
    {
        name: 'Central African Republic',
        phoneCode: '236',
    },
    {
        name: 'Chad',
        phoneCode: '235',
    },
    {
        name: 'Chile',
        phoneCode: '56',
    },
    {
        name: 'China',
        phoneCode: '86',
    },
    {
        name: 'Christmas Island',
        phoneCode: '61',
    },
    {
        name: 'Cocos (Keeling) Islands',
        phoneCode: '61',
    },
    {
        name: 'Colombia',
        phoneCode: '57',
    },
    {
        name: 'Comoros',
        phoneCode: '269',
    },
    {
        name: 'Congo',
        phoneCode: '242',
    },
    {
        name: 'Congo (Democratic Republic of the)',
        phoneCode: '243',
    },
    {
        name: 'Cook Islands',
        phoneCode: '682',
    },
    {
        name: 'Costa Rica',
        phoneCode: '506',
    },
    {
        name: 'Croatia',
        phoneCode: '385',
    },
    {
        name: 'Cuba',
        phoneCode: '53',
    },
    {
        name: 'Curaçao',
        phoneCode: '599',
    },
    {
        name: 'Cyprus',
        phoneCode: '357',
    },
    {
        name: 'Czech Republic',
        phoneCode: '420',
    },
    {
        name: 'Denmark',
        phoneCode: '45',
    },
    {
        name: 'Djibouti',
        phoneCode: '253',
    },
    {
        name: 'Dominica',
        phoneCode: '1767',
    },
    {
        name: 'Dominican Republic',
        phoneCode: '1809,1829,1849',
    },
    {
        name: 'Ecuador',
        phoneCode: '593',
    },
    {
        name: 'Egypt',
        phoneCode: '20',
    },
    {
        name: 'El Salvador',
        phoneCode: '503',
    },
    {
        name: 'Equatorial Guinea',
        phoneCode: '240',
    },
    {
        name: 'Eritrea',
        phoneCode: '291',
    },
    {
        name: 'Estonia',
        phoneCode: '372',
    },
    {
        name: 'Ethiopia',
        phoneCode: '251',
    },
    {
        name: 'Falkland Islands (Malvinas)',
        phoneCode: '500',
    },
    {
        name: 'Faroe Islands',
        phoneCode: '298',
    },
    {
        name: 'Fiji',
        phoneCode: '679',
    },
    {
        name: 'Finland',
        phoneCode: '358',
    },
    {
        name: 'France',
        phoneCode: '33',
    },
    {
        name: 'French Guiana',
        phoneCode: '594',
    },
    {
        name: 'French Polynesia',
        phoneCode: '689',
    },
    {
        name: 'French Southern Territories',
        phoneCode: '',
    },
    {
        name: 'Gabon',
        phoneCode: '241',
    },
    {
        name: 'Gambia',
        phoneCode: '220',
    },
    {
        name: 'Georgia',
        phoneCode: '995',
    },
    {
        name: 'Germany',
        phoneCode: '49',
    },
    {
        name: 'Ghana',
        phoneCode: '233',
    },
    {
        name: 'Gibraltar',
        phoneCode: '350',
    },
    {
        name: 'Greece',
        phoneCode: '30',
    },
    {
        name: 'Greenland',
        phoneCode: '299',
    },
    {
        name: 'Grenada',
        phoneCode: '1473',
    },
    {
        name: 'Guadeloupe',
        phoneCode: '590',
    },
    {
        name: 'Guam',
        phoneCode: '1671',
    },
    {
        name: 'Guatemala',
        phoneCode: '502',
    },
    {
        name: 'Guernsey',
        phoneCode: '44',
    },
    {
        name: 'Guinea',
        phoneCode: '224',
    },
    {
        name: 'Guinea-Bissau',
        phoneCode: '245',
    },
    {
        name: 'Guyana',
        phoneCode: '592',
    },
    {
        name: 'Haiti',
        phoneCode: '509',
    },
    {
        name: 'Heard Island and McDonald Islands',
        phoneCode: '',
    },
    {
        name: 'Holy See',
        phoneCode: '379',
    },
    {
        name: 'Honduras',
        phoneCode: '504',
    },
    {
        name: 'Hong Kong',
        phoneCode: '852',
    },
    {
        name: 'Hungary',
        phoneCode: '36',
    },
    {
        name: 'Iceland',
        phoneCode: '354',
    },
    {
        name: 'India',
        phoneCode: '91',
    },
    {
        name: 'Indonesia',
        phoneCode: '62',
    },
    {
        name: 'Côte d\'Ivoire',
        phoneCode: '225',
    },
    {
        name: 'Iran (Islamic Republic of)',
        phoneCode: '98',
    },
    {
        name: 'Iraq',
        phoneCode: '964',
    },
    {
        name: 'Ireland',
        phoneCode: '353',
    },
    {
        name: 'Isle of Man',
        phoneCode: '44',
    },
    {
        name: 'Israel',
        phoneCode: '972',
    },
    {
        name: 'Italy',
        phoneCode: '39',
    },
    {
        name: 'Jamaica',
        phoneCode: '1876',
    },
    {
        name: 'Japan',
        phoneCode: '81',
    },
    {
        name: 'Jersey',
        phoneCode: '44',
    },
    {
        name: 'Jordan',
        phoneCode: '962',
    },
    {
        name: 'Kazakhstan',
        phoneCode: '76,77',
    },
    {
        name: 'Kenya',
        phoneCode: '254',
    },
    {
        name: 'Kiribati',
        phoneCode: '686',
    },
    {
        name: 'Kuwait',
        phoneCode: '965',
    },
    {
        name: 'Kyrgyzstan',
        phoneCode: '996',
    },
    {
        name: 'Lao People\'s Democratic Republic',
        phoneCode: '856',
    },
    {
        name: 'Latvia',
        phoneCode: '371',
    },
    {
        name: 'Lebanon',
        phoneCode: '961',
    },
    {
        name: 'Lesotho',
        phoneCode: '266',
    },
    {
        name: 'Liberia',
        phoneCode: '231',
    },
    {
        name: 'Libya',
        phoneCode: '218',
    },
    {
        name: 'Liechtenstein',
        phoneCode: '423',
    },
    {
        name: 'Lithuania',
        phoneCode: '370',
    },
    {
        name: 'Luxembourg',
        phoneCode: '352',
    },
    {
        name: 'Macao',
        phoneCode: '853',
    },
    {
        name: 'Macedonia (the former Yugoslav Republic of)',
        phoneCode: '389',
    },
    {
        name: 'Madagascar',
        phoneCode: '261',
    },
    {
        name: 'Malawi',
        phoneCode: '265',
    },
    {
        name: 'Malaysia',
        phoneCode: '60',
    },
    {
        name: 'Maldives',
        phoneCode: '960',
    },
    {
        name: 'Mali',
        phoneCode: '223',
    },
    {
        name: 'Malta',
        phoneCode: '356',
    },
    {
        name: 'Marshall Islands',
        phoneCode: '692',
    },
    {
        name: 'Martinique',
        phoneCode: '596',
    },
    {
        name: 'Mauritania',
        phoneCode: '222',
    },
    {
        name: 'Mauritius',
        phoneCode: '230',
    },
    {
        name: 'Mayotte',
        phoneCode: '262',
    },
    {
        name: 'Mexico',
        phoneCode: '52',
    },
    {
        name: 'Micronesia (Federated States of)',
        phoneCode: '691',
    },
    {
        name: 'Moldova (Republic of)',
        phoneCode: '373',
    },
    {
        name: 'Monaco',
        phoneCode: '377',
    },
    {
        name: 'Mongolia',
        phoneCode: '976',
    },
    {
        name: 'Montenegro',
        phoneCode: '382',
    },
    {
        name: 'Montserrat',
        phoneCode: '1664',
    },
    {
        name: 'Morocco',
        phoneCode: '212',
    },
    {
        name: 'Mozambique',
        phoneCode: '258',
    },
    {
        name: 'Myanmar',
        phoneCode: '95',
    },
    {
        name: 'Namibia',
        phoneCode: '264',
    },
    {
        name: 'Nauru',
        phoneCode: '674',
    },
    {
        name: 'Nepal',
        phoneCode: '977',
    },
    {
        name: 'Netherlands',
        phoneCode: '31',
    },
    {
        name: 'New Caledonia',
        phoneCode: '687',
    },
    {
        name: 'New Zealand',
        phoneCode: '64',
    },
    {
        name: 'Nicaragua',
        phoneCode: '505',
    },
    {
        name: 'Niger',
        phoneCode: '227',
    },
    {
        name: 'Nigeria',
        phoneCode: '234',
    },
    {
        name: 'Niue',
        phoneCode: '683',
    },
    {
        name: 'Norfolk Island',
        phoneCode: '672',
    },
    {
        name: 'Korea (Democratic People\'s Republic of)',
        phoneCode: '850',
    },
    {
        name: 'Northern Mariana Islands',
        phoneCode: '1670',
    },
    {
        name: 'Norway',
        phoneCode: '47',
    },
    {
        name: 'Oman',
        phoneCode: '968',
    },
    {
        name: 'Pakistan',
        phoneCode: '92',
    },
    {
        name: 'Palau',
        phoneCode: '680',
    },
    {
        name: 'Palestine, State of',
        phoneCode: '970',
    },
    {
        name: 'Panama',
        phoneCode: '507',
    },
    {
        name: 'Papua New Guinea',
        phoneCode: '675',
    },
    {
        name: 'Paraguay',
        phoneCode: '595',
    },
    {
        name: 'Peru',
        phoneCode: '51',
    },
    {
        name: 'Philippines',
        phoneCode: '63',
    },
    {
        name: 'Pitcairn',
        phoneCode: '64',
    },
    {
        name: 'Poland',
        phoneCode: '48',
    },
    {
        name: 'Portugal',
        phoneCode: '351',
    },
    {
        name: 'Puerto Rico',
        phoneCode: '1787,1939',
    },
    {
        name: 'Qatar',
        phoneCode: '974',
    },
    {
        name: 'Republic of Kosovo',
        phoneCode: '383',
    },
    {
        name: 'Réunion',
        phoneCode: '262',
    },
    {
        name: 'Romania',
        phoneCode: '40',
    },
    {
        name: 'Russian Federation',
        phoneCode: '7',
    },
    {
        name: 'Rwanda',
        phoneCode: '250',
    },
    {
        name: 'Saint Barthélemy',
        phoneCode: '590',
    },
    {
        name: 'Saint Helena, Ascension and Tristan da Cunha',
        phoneCode: '290',
    },
    {
        name: 'Saint Kitts and Nevis',
        phoneCode: '1869',
    },
    {
        name: 'Saint Lucia',
        phoneCode: '1758',
    },
    {
        name: 'Saint Martin (French part)',
        phoneCode: '590',
    },
    {
        name: 'Saint Pierre and Miquelon',
        phoneCode: '508',
    },
    {
        name: 'Saint Vincent and the Grenadines',
        phoneCode: '1784',
    },
    {
        name: 'Samoa',
        phoneCode: '685',
    },
    {
        name: 'San Marino',
        phoneCode: '378',
    },
    {
        name: 'Sao Tome and Principe',
        phoneCode: '239',
    },
    {
        name: 'Saudi Arabia',
        phoneCode: '966',
    },
    {
        name: 'Senegal',
        phoneCode: '221',
    },
    {
        name: 'Serbia',
        phoneCode: '381',
    },
    {
        name: 'Seychelles',
        phoneCode: '248',
    },
    {
        name: 'Sierra Leone',
        phoneCode: '232',
    },
    {
        name: 'Singapore',
        phoneCode: '65',
    },
    {
        name: 'Sint Maarten (Dutch part)',
        phoneCode: '1721',
    },
    {
        name: 'Slovakia',
        phoneCode: '421',
    },
    {
        name: 'Slovenia',
        phoneCode: '386',
    },
    {
        name: 'Solomon Islands',
        phoneCode: '677',
    },
    {
        name: 'Somalia',
        phoneCode: '252',
    },
    {
        name: 'South Africa',
        phoneCode: '27',
    },
    {
        name: 'South Georgia and the South Sandwich Islands',
        phoneCode: '500',
    },
    {
        name: 'Korea (Republic of)',
        phoneCode: '82',
    },
    {
        name: 'South Sudan',
        phoneCode: '211',
    },
    {
        name: 'Spain',
        phoneCode: '34',
    },
    {
        name: 'Sri Lanka',
        phoneCode: '94',
    },
    {
        name: 'Sudan',
        phoneCode: '249',
    },
    {
        name: 'Suriname',
        phoneCode: '597',
    },
    {
        name: 'Svalbard and Jan Mayen',
        phoneCode: '4779',
    },
    {
        name: 'Swaziland',
        phoneCode: '268',
    },
    {
        name: 'Sweden',
        phoneCode: '46',
    },
    {
        name: 'Switzerland',
        phoneCode: '41',
    },
    {
        name: 'Syrian Arab Republic',
        phoneCode: '963',
    },
    {
        name: 'Taiwan',
        phoneCode: '886',
    },
    {
        name: 'Tajikistan',
        phoneCode: '992',
    },
    {
        name: 'Tanzania, United Republic of',
        phoneCode: '255',
    },
    {
        name: 'Thailand',
        phoneCode: '66',
    },
    {
        name: 'Timor-Leste',
        phoneCode: '670',
    },
    {
        name: 'Togo',
        phoneCode: '228',
    },
    {
        name: 'Tokelau',
        phoneCode: '690',
    },
    {
        name: 'Tonga',
        phoneCode: '676',
    },
    {
        name: 'Trinidad and Tobago',
        phoneCode: '1868',
    },
    {
        name: 'Tunisia',
        phoneCode: '216',
    },
    {
        name: 'Turkey',
        phoneCode: '90',
    },
    {
        name: 'Turkmenistan',
        phoneCode: '993',
    },
    {
        name: 'Turks and Caicos Islands',
        phoneCode: '1649',
    },
    {
        name: 'Tuvalu',
        phoneCode: '688',
    },
    {
        name: 'Uganda',
        phoneCode: '256',
    },
    {
        name: 'Ukraine',
        phoneCode: '380',
    },
    {
        name: 'United Arab Emirates',
        phoneCode: '971',
    },
    {
        name: 'United Kingdom of Great Britain and Northern Ireland',
        phoneCode: '44',
    },
    {
        name: 'United States of America',
        phoneCode: '1',
    },
    {
        name: 'Uruguay',
        phoneCode: '598',
    },
    {
        name: 'Uzbekistan',
        phoneCode: '998',
    },
    {
        name: 'Vanuatu',
        phoneCode: '678',
    },
    {
        name: 'Venezuela (Bolivarian Republic of)',
        phoneCode: '58',
    },
    {
        name: 'Viet Nam',
        phoneCode: '84',
    },
    {
        name: 'Wallis and Futuna',
        phoneCode: '681',
    },
    {
        name: 'Western Sahara',
        phoneCode: '212',
    },
    {
        name: 'Yemen',
        phoneCode: '967',
    },
    {
        name: 'Zambia',
        phoneCode: '260',
    },
    {
        name: 'Zimbabwe',
        phoneCode: '263',
    },
];