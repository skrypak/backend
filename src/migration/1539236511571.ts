import { MigrationInterface, QueryRunner } from "typeorm";

export class User1539236511571 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
       await queryRunner.query("Insert into user (email, password_hash, first_name, last_name, cityId, departmentId, positionId) values ('admin@mail.com', '$2b$10$DhOmMMUwtAvyBxG7zji.ouZ0HzTwFfBsH.gjpYF8HHvqbjnCq3a1O', 'admin', 'admin', 1, 1, 1)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query("Delete from user where email='admin@mail.com'");           
    }

}
