import {MigrationInterface, QueryRunner} from "typeorm";

export class TypeToStatus1541747625211 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `idp` CHANGE `type` `status` enum ('IN_PROGRESS', 'DRAFT', 'IN_REVIEW', 'FINAL_REVIEW', 'COMPLETED') NOT NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_start_job` `date_start_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_end_job` `date_end_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `client` CHANGE `deletedAt` `deletedAt` datetime NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_start_job` `date_start_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_end_job` `date_end_job` datetime NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_end_job` `date_end_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_start_job` `date_start_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `client` CHANGE `deletedAt` `deletedAt` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_end_job` `date_end_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_start_job` `date_start_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `idp` CHANGE `status` `type` enum ('IN_PROGRESS', 'DRAFT', 'IN_REVIEW', 'FINAL_REVIEW', 'COMPLETED') NOT NULL");
    }

}
