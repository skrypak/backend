import {MigrationInterface, QueryRunner} from "typeorm";

export class UserConfig1545130394196 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query("CREATE TABLE `user_config` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `sidebarCollapse` tinyint NOT NULL DEFAULT 0, `userId` int NULL, UNIQUE INDEX `REL_50aa50cd542e360ea75bf4eaa7` (`userId`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
    await queryRunner.query("INSERT INTO user_config (userId) VALUES (1)");
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query("DELETE FROM user_config WHERE userId=1");
    await queryRunner.query("DROP TABLE `user_config`");
  }
  
}
