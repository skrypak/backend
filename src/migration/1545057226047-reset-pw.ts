import {MigrationInterface, QueryRunner} from "typeorm";

export class resetPw1545057226047 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `user_reset_password` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `reset_token` varchar(255) NOT NULL, `expire_date` datetime NOT NULL, `used` tinyint NOT NULL, `userId` int NULL, UNIQUE INDEX `IDX_f763e730d2621107f6c7be9a2b` (`reset_token`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `user` CHANGE `password_hash` `password_hash` varchar(255) NULL");
        await queryRunner.query("ALTER TABLE `user_reset_password` ADD CONSTRAINT `FK_c88e6b917ea23eba24be9d4c32a` FOREIGN KEY (`userId`) REFERENCES `user`(`id`)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user_reset_password` DROP FOREIGN KEY `FK_c88e6b917ea23eba24be9d4c32a`");
        await queryRunner.query("ALTER TABLE `user` CHANGE `password_hash` `password_hash` varchar(255) NOT NULL");
        await queryRunner.query("DROP INDEX `IDX_f763e730d2621107f6c7be9a2b` ON `user_reset_password`");
        await queryRunner.query("DROP TABLE `user_reset_password`");
    }
}
