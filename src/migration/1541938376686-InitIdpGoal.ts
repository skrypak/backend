import {MigrationInterface, QueryRunner} from "typeorm";

export class InitIdpGoal1541938376686 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `idp_goal` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `start` date NULL, `end` date NULL, `status` enum ('IN_PROGRESS', 'ON_HOLD', 'IN_REVIEW', 'COMPLETED') NOT NULL, `title` varchar(255) NOT NULL, `description` text NOT NULL, `idpId` int NULL, `technologyTypeId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_start_job` `date_start_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_end_job` `date_end_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `client` CHANGE `deletedAt` `deletedAt` datetime NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_start_job` `date_start_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_end_job` `date_end_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `idp_goal` ADD CONSTRAINT `FK_d7c61ffe40cad3a488b06c8a12f` FOREIGN KEY (`idpId`) REFERENCES `idp`(`id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `idp_goal` ADD CONSTRAINT `FK_67c759bf6e41cdfe086a3bd4861` FOREIGN KEY (`technologyTypeId`) REFERENCES `technology_type`(`id`) ON DELETE CASCADE");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `idp_goal` DROP FOREIGN KEY `FK_67c759bf6e41cdfe086a3bd4861`");
        await queryRunner.query("ALTER TABLE `idp_goal` DROP FOREIGN KEY `FK_d7c61ffe40cad3a488b06c8a12f`");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_end_job` `date_end_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_start_job` `date_start_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `client` CHANGE `deletedAt` `deletedAt` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_end_job` `date_end_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_start_job` `date_start_job` datetime(0) NULL");
        await queryRunner.query("DROP TABLE `idp_goal`");
    }

}
