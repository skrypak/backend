import {MigrationInterface, QueryRunner} from "typeorm";

export class FixProjectTypes1544184267736 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `project` MODIFY `type` enum ('INTERNAL', 'COMMERCE', 'Design', 'Development') NOT NULL");
        await queryRunner.query("UPDATE `project` SET `type`='INTERNAL' WHERE NOT `type` IN ('INTERNAL', 'COMMERCE')");
        await queryRunner.query("ALTER TABLE `project` MODIFY `type` enum ('INTERNAL', 'COMMERCE') NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `project` MODIFY `type` enum ('INTERNAL', 'COMMERCE', 'Design', 'Development') NOT NULL");
        await queryRunner.query("UPDATE `project` SET `type`='INTERNAL' WHERE NOT `type` IN ('INTERNAL', 'COMMERCE')");
        await queryRunner.query("ALTER TABLE `project` MODIFY `type` enum ('INTERNAL', 'COMMERCE') NOT NULL");
    }

}
