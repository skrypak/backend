import {MigrationInterface, QueryRunner} from "typeorm";

export class addCooperationType1544172956982 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `project` ADD `cooperation` enum ('FREELANCE_OUTSTAFF', 'FREELANCE_OUTSOURCE', 'COMPANY_OUTSTAFF', 'COMPANY_OUTSOURCE') NOT NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_start_job` `date_start_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_end_job` `date_end_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `client` CHANGE `deletedAt` `deletedAt` datetime NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_start_job` `date_start_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_end_job` `date_end_job` datetime NULL");
        await queryRunner.query("INSERT IGNORE INTO `project` SET `cooperation`='FREELANCE_OUTSTAFF'");
        await queryRunner.query("UPDATE `project` SET `type`='INTERNAL' WHERE NOT `type` IN ('INTERNAL', 'COMMERCE')");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `project` ADD `cooperation` enum ('FREELANCE_OUTSTAFF', 'FREELANCE_OUTSOURCE', 'COMPANY_OUTSTAFF', 'COMPANY_OUTSOURCE') NOT NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_end_job` `date_end_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_start_job` `date_start_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `client` CHANGE `deletedAt` `deletedAt` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_end_job` `date_end_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_start_job` `date_start_job` datetime(0) NULL");
        await queryRunner.query("INSERT IGNORE INTO `project` SET `cooperation`='FREELANCE_OUTSTAFF'");
        await queryRunner.query("UPDATE `project` SET `type`='INTERNAL' WHERE NOT `type` IN ('INTERNAL', 'COMMERCE')");
    }

}
