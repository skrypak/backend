import {MigrationInterface, QueryRunner} from "typeorm";

export class AddDefaultValues1542009519351 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `idp` ADD `name` varchar(100) NOT NULL");
        await queryRunner.query("ALTER TABLE `idp` CHANGE `status` `status` enum ('IN_PROGRESS', 'DRAFT', 'IN_REVIEW', 'FINAL_REVIEW', 'COMPLETED') NOT NULL DEFAULT 'DRAFT'");
        await queryRunner.query("ALTER TABLE `idp_goal` CHANGE `status` `status` enum ('IN_PROGRESS', 'ON_HOLD', 'IN_REVIEW', 'COMPLETED') NOT NULL DEFAULT 'ON_HOLD'");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_start_job` `date_start_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_end_job` `date_end_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `client` CHANGE `deletedAt` `deletedAt` datetime NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_start_job` `date_start_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_end_job` `date_end_job` datetime NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_end_job` `date_end_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_start_job` `date_start_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `client` CHANGE `deletedAt` `deletedAt` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_end_job` `date_end_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_start_job` `date_start_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `idp_goal` CHANGE `status` `status` enum ('IN_PROGRESS', 'ON_HOLD', 'IN_REVIEW', 'COMPLETED') NOT NULL");
        await queryRunner.query("ALTER TABLE `idp` CHANGE `status` `status` enum ('IN_PROGRESS', 'DRAFT', 'IN_REVIEW', 'FINAL_REVIEW', 'COMPLETED') NOT NULL");
        await queryRunner.query("ALTER TABLE `idp` DROP COLUMN `name`");
    }

}
