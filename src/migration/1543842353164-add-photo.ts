import {MigrationInterface, QueryRunner} from "typeorm";

export class addPhoto1543842353164 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `project` ADD `image` varchar(255) NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `project` DROP COLUMN `image`");
    }

}
