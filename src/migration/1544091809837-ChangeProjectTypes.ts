import {MigrationInterface, QueryRunner} from "typeorm";

export class ChangeProjectTypes1544091809837 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `project` MODIFY `type` enum ('INTERNAL', 'COMMERCE') NOT NULL");
        await queryRunner.query("UPDATE `project` SET `type`='INTERNAL' WHERE NOT `type` IN ('INTERNAL', 'COMMERCE')");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `project` MODIFY `type` enum ('INTERNAL', 'COMMERCE') NOT NULL");
        await queryRunner.query("UPDATE `project` SET `type`='INTERNAL' WHERE NOT `type` IN ('INTERNAL', 'COMMERCE')");
    }
}
