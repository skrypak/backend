import {MigrationInterface, QueryRunner} from 'typeorm';

export class userRole1544519104400 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('CREATE TABLE `project_user_role` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, UNIQUE INDEX `IDX_045a944b3249af0e0079a635ff` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB');
        await queryRunner.query('CREATE TABLE `project_user` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `start` datetime NULL, `end` datetime NULL, `involvement` int NOT NULL, `roleId` int NULL, `projectId` int NULL, `userId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB');
        await queryRunner.query('ALTER TABLE `project_user` ADD CONSTRAINT `FK_2241ed7471b3af553b764a22fb9` FOREIGN KEY (`roleId`) REFERENCES `project_user_role`(`id`)');
        await queryRunner.query('ALTER TABLE `project_user` ADD CONSTRAINT `FK_be4e7ad73afd703f94b8866eb6b` FOREIGN KEY (`projectId`) REFERENCES `project`(`id`)');
        await queryRunner.query('ALTER TABLE `project_user` ADD CONSTRAINT `FK_8d75193a81f827ba8d58575e637` FOREIGN KEY (`userId`) REFERENCES `user`(`id`)');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `project_user` DROP FOREIGN KEY `FK_8d75193a81f827ba8d58575e637`');
        await queryRunner.query('ALTER TABLE `project_user` DROP FOREIGN KEY `FK_be4e7ad73afd703f94b8866eb6b`');
        await queryRunner.query('ALTER TABLE `project_user` DROP FOREIGN KEY `FK_2241ed7471b3af553b764a22fb9`');
        await queryRunner.query('DROP TABLE `project_user`');
        await queryRunner.query('DROP INDEX `IDX_045a944b3249af0e0079a635ff` ON `project_user_role`');
        await queryRunner.query('DROP TABLE `project_user_role`');
    }

}
