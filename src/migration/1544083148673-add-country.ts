import { MigrationInterface, QueryRunner } from 'typeorm';
import { countryArr } from './country.dump';

export class addCountry1544083148673 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('CREATE TABLE `country` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `name` varchar(255) NOT NULL, `phoneCode` varchar(255) NOT NULL, UNIQUE INDEX `IDX_2c5aa339240c0c3ae97fcc9dc4` (`name`), PRIMARY KEY (`id`)) ENGINE=InnoDB');
        await queryRunner.connection
            .createQueryBuilder()
            .insert()
            .into('country')
            .values(countryArr)
            .execute();
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.dropIndex('country', 'IDX_2c5aa339240c0c3ae97fcc9dc4');
        await queryRunner.dropTable('country');
    }

}
