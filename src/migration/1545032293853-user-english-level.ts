import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserEnglishLevel1545032293853 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query("ALTER TABLE `user` ADD `english_level_id` int NULL");
    await queryRunner.query("CREATE TABLE `user_english_level` (`id` int NOT NULL AUTO_INCREMENT, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `level` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
    await queryRunner.query("INSERT INTO `user_english_level` (level) values ('A1 (Beginner)')");
    await queryRunner.query("INSERT INTO `user_english_level` (level) values ('A2 (Elementary English)')");
    await queryRunner.query("INSERT INTO `user_english_level` (level) values ('B1 (Intermediate English)')");
    await queryRunner.query("INSERT INTO `user_english_level` (level) values ('B2 (Upper-Intermediate English)')");
    await queryRunner.query("INSERT INTO `user_english_level` (level) values ('C1 (Advanced English)')");
    await queryRunner.query("INSERT INTO `user_english_level` (level) values ('C2 (Proficiency English)')");
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query("ALTER TABLE `user` DROP COLUMN `english_level_id`");
    await queryRunner.query("delete from 'user_english_level'");
    await queryRunner.query("drop table 'user_english_level'");
  }

}
