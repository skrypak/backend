import {MigrationInterface, QueryRunner} from "typeorm";

export class ChangeStartEnd1541745799278 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_start_job` `date_start_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_end_job` `date_end_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `client` CHANGE `deletedAt` `deletedAt` datetime NULL");
        await queryRunner.query("ALTER TABLE `idp` DROP COLUMN `start`");
        await queryRunner.query("ALTER TABLE `idp` ADD `start` date NULL");
        await queryRunner.query("ALTER TABLE `idp` DROP COLUMN `end`");
        await queryRunner.query("ALTER TABLE `idp` ADD `end` date NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_start_job` `date_start_job` datetime NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_end_job` `date_end_job` datetime NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_end_job` `date_end_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `user` CHANGE `date_start_job` `date_start_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `idp` DROP COLUMN `end`");
        await queryRunner.query("ALTER TABLE `idp` ADD `end` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `idp` DROP COLUMN `start`");
        await queryRunner.query("ALTER TABLE `idp` ADD `start` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `client` CHANGE `deletedAt` `deletedAt` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_end_job` `date_end_job` datetime(0) NULL");
        await queryRunner.query("ALTER TABLE `project` CHANGE `date_start_job` `date_start_job` datetime(0) NULL");
    }

}
