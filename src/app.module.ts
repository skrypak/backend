import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { EventsModule } from './events/events.module';
import { ORM_CONFIG, MAILER_CONFIG } from './config';
import { CoreModule } from './core/core.module';
import { CityModule } from './city/city.module';
import { DepartmentModule } from './department/department.module';
import { TechnologyModule } from './technology/technology.module';
import { TechnologyTypeModule } from './technology-type/technology-type.module';
import { SocialModule } from './social/social.module';
import { PositionModule } from 'position/position.module';
import { ProjectStatusModule } from 'project-status/project-status.module';
import { ProjectModule } from './project/project.module';
import { ClientStatusModule } from './client-status/client-status.module';
import { ClientModule } from './client/client.module';
import { ProjectToolsModule } from 'project-tools/project-tools.module';
import { UserRoleModule } from 'user-role/user-role.module';
import { IdpModule } from './idp/idp.module';
import { IdpGoalModule } from './idp-goal/idp-goal.module';
import { CountryModule } from 'contry/country.module';
import { ProjectUserRoleModule } from './project-user-role/project-user-role.module';
import { ProjectUserModule } from 'project-user/project-user.module';
import { MailerModule } from '@nest-modules/mailer';
import { UserResetPasswordModule } from 'user-reset-password/user-reset-password.module';
import { UserEnglishLevelModule } from 'user-english-level/user-english-level.module';
import { UserConfigModule } from 'user-config/user-config.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(ORM_CONFIG),
    MailerModule.forRoot(MAILER_CONFIG),
    CoreModule,
    AuthModule,
    UserModule,
    UserRoleModule,
    EventsModule,
    CityModule,
    DepartmentModule,
    TechnologyModule,
    TechnologyTypeModule,
    SocialModule,
    PositionModule,
    ProjectModule,
    ProjectStatusModule,
    ClientStatusModule,
    ClientModule,
    ProjectToolsModule,
    IdpModule,
    IdpGoalModule,
    CountryModule,
    ProjectUserRoleModule,
    ProjectUserModule,
    UserResetPasswordModule,
    UserEnglishLevelModule,
    UserConfigModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule { }
