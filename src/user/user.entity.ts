import { Entity, Column, JoinColumn, ManyToOne, ManyToMany, OneToMany, JoinTable, OneToOne } from 'typeorm';
import { City } from '../city/city.entity';
import { Gender } from '../common/enums/gender.enum';
import { Department } from '../department/department.entity';
import { Exclude } from 'class-transformer';
import { Social } from '../social/social.entity';
import { Position } from '../position/position.entity';
import { BaseEntity } from '../common/entity/base.entity';
import { Project } from '../project/project.entity';
import { Client } from '../client/client.entity';
import { UserRole } from '../user-role/user-role.entity';
import { Idp } from '../idp/idp.entity';
import { Technology } from '../technology/technology.entity';
import { ProjectUser } from '../project-user/project-user.entity';
import { UserResetPassword } from '../user-reset-password/user-reset-password.entity';
import { UserEnglishLevel } from '../user-english-level/user-english-level.entity';

@Entity()
export class User extends BaseEntity {

  @Column({ length: 255, select: false, nullable: true })
  @Exclude()
  password_hash: string;

  @Column({ unique: true, length: 255 })
  email: string;

  @Column({ length: 255, nullable: true })
  first_name: string;

  @Column({ length: 255, nullable: true })
  last_name: string;

  @Column({ length: 45, nullable: true, unique: true })
  phone: string;

  @Column({ length: 255, nullable: true })
  photo: string;

  @Column()
  level: number;

  @ManyToOne(type => City, city => city.users)
  @JoinColumn()
  city: City;

  @ManyToOne(type => Department, department => department.users)
  @JoinColumn()
  department: Department;

  @ManyToOne(type => Position, position => position.users)
  @JoinColumn()
  position: Position;

  @OneToMany(type => Project, project => project.saleManager)
  projectsForSale: Project[];

  @OneToMany(type => Client, client => client.saleManager)
  clients: Client[];

  @OneToMany(type => ProjectUser, projectUser => projectUser.user)
  projectsRef: ProjectUser[];

  @ManyToMany(type => Technology, technology => technology.users)
  @JoinTable({name: 'user_has_technology'})
  technologies: Technology[];

  @OneToMany(type => UserResetPassword, resetPassword => resetPassword.user)
  resetPassword: UserResetPassword[];

  @Column('enum', { enum: Gender, nullable: true })
  gender: Gender;

  @ManyToOne(type => UserRole, role => role.users, { nullable: false })
  role: UserRole;

  @Column({ nullable: true })
  experience: number = 0;

  @Column({ type: 'text', nullable: true })
  qualities: string;

  @Column({ type: 'text', nullable: true })
  ssh_key: string;

  @Column({ type: Date, nullable: true })
  date_start_job: Date;

  @Column({ type: Date, nullable: true })
  date_end_job: Date;

  @OneToMany(type => Social, social => social.user)
  social: Social[];

  @OneToMany(type => Idp, idp => idp.user)
  idp: Idp[];

  @ManyToOne(type => UserEnglishLevel, level => level.users)
  @JoinColumn({ name: 'english_level_id' })
  english_level: UserEnglishLevel;

  constructor(partial: Partial<User>) {
    super();
    Object.assign(this, partial);
  }
}
