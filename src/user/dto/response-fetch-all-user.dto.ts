import { IsNumber, IsArray } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { User } from 'user/user.entity';
import { ResponseUserInfoDto } from './response-user.dto';

export class ResponseUserDto {
    @IsNumber() @ApiModelProperty({ example: 10 }) readonly count: number;
    @IsNumber() @ApiModelProperty({ example: 1 }) readonly page: number;
    @IsNumber() @ApiModelProperty({ example: 100 }) readonly total: number;
    @IsArray()  @ApiModelProperty({ type: ResponseUserInfoDto, isArray: true }) readonly data: User[];
}