import { IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateUserPhotoDto {
  @IsString() @ApiModelProperty() readonly userId: number;
  @IsString() @ApiModelProperty() readonly filename: string;
  }
