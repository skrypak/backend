import { IsEmail } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ResetUserPasswordDto {
  @IsEmail()
  @ApiModelProperty()
  readonly email: string;
}
