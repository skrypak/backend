import { IsNumber, IsString, IsEmail, IsDateString, IsEnum, IsOptional, Min, Max, IsArray, ValidateNested } from 'class-validator';
import { Gender } from 'common/enums/gender.enum';
import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { UpdateTechnologyDto } from 'technology/dto/update-technology.dto';
import { UpdateSocialDto } from 'social/dto/update-social.dto';

export class UpdateUserDto {
  @IsNumber() @ApiModelProperty() readonly id: number;
  @IsEmail() @ApiModelProperty() readonly email: string;
  @IsNumber() @ApiModelProperty() readonly department: number;
  @IsNumber() @ApiModelProperty() readonly position: number;
  @IsString() @ApiModelProperty() readonly first_name: string;
  @IsString() @ApiModelProperty() readonly last_name: string;
  @IsString() @ApiModelProperty() readonly phone: string;
  @IsNumber() @ApiModelProperty() readonly city: number;
  @IsString() @ApiModelProperty() readonly photo: string;
  @IsNumber() @Min(1) @Max(10) @ApiModelProperty({ description: 'Value between 1 and 10' }) readonly level: number;
  @IsEnum(Gender) @ApiModelProperty({ enum: Gender }) readonly gender: Gender;
  @IsNumber() @ApiModelProperty() readonly role: number;
  @IsNumber() @ApiModelProperty() readonly experience: number;
  @IsArray() @ValidateNested() @Type(() => UpdateTechnologyDto) @ApiModelProperty() readonly technologies: UpdateTechnologyDto[];
  @IsString() @ApiModelProperty() readonly qualities: string;
  @IsString() @IsOptional() @ApiModelProperty() readonly ssh_key: string;
  @IsDateString() @ApiModelProperty() readonly date_start_job: Date;
  @IsDateString() @IsOptional() @ApiModelProperty({ required: false }) readonly date_end_job?: Date;
  @IsArray() @ValidateNested() readonly social: UpdateSocialDto[];
  @IsNumber() @ApiModelProperty() readonly english_level: number;
}
