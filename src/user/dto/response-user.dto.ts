import { Gender } from 'common/enums/gender.enum';
import { ApiModelProperty } from '@nestjs/swagger';
import { ResponseDepartmentDto } from 'department/dto/response-department.dto';
import { ResponsePositionDto } from 'position/dto/response-position.dto';
import { ResponseCityDto } from 'city/dto/response-city.dto';
import { RawTechnology } from 'technology/dto/raw-technology.dto';
import { ResponseUserRoleDto } from 'user-role/dto/response-user-role.dto';
import { ResponseSocialDto } from 'social/dto/response-social.dto';

export class ResponseUserInfoDto {

  @ApiModelProperty({ example: 1 })
  readonly id: number;

  @ApiModelProperty({ example: 'mail@example.com' })
  readonly email: string;

  @ApiModelProperty({ example: 'Admin' })
  readonly first_name?: string;

  @ApiModelProperty({ example: 'User' })
  readonly last_name?: string;

  @ApiModelProperty({ enum: Gender, example: Gender.MALE })
  readonly gender: Gender;

  @ApiModelProperty({ type: ResponseDepartmentDto })
  readonly department: ResponseDepartmentDto;

  @ApiModelProperty({ type: ResponsePositionDto })
  readonly position: ResponsePositionDto;

  @ApiModelProperty({ type: ResponseCityDto })
  readonly city: ResponseCityDto;

  @ApiModelProperty({ description: 'Value between 1 and 10', example: 7 })
  readonly level: number;

  @ApiModelProperty({ type: RawTechnology, isArray: true })
  readonly technologies: RawTechnology[];

  @ApiModelProperty({ example: '0 123 45 67 890' })
  readonly phone: string;

  @ApiModelProperty({ example: 'http://s3-external-1.amazonaws.com/bucket/my-image.jpg' })
  readonly photo: string;

  @ApiModelProperty({ example: 2 })
  readonly experience: number;

  @ApiModelProperty({ example: 'Creative, open-minded' })
  readonly qualities: string;

  @ApiModelProperty({ example: '2018-10-18T13:10:52.000Z' })
  readonly date_start_job: Date;

  @ApiModelProperty({ example: '2018-10-18T13:10:52.000Z' })
  readonly date_end_job: Date;

  @ApiModelProperty({ type: ResponseUserRoleDto })
  readonly role: ResponseUserRoleDto;

  @ApiModelProperty({ type: ResponseSocialDto, isArray: true })
  readonly social: ResponseSocialDto[];
}
