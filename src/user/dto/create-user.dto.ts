import { IsString, IsEmail, IsOptional, IsNumber, IsDateString, IsEnum, IsArray, ValidateNested, Min, Max } from 'class-validator';
import { Gender } from 'common/enums/gender.enum';
import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { CreateSocialDto } from 'social/dto/create-social.dto';
import { UpdateTechnologyDto } from 'technology/dto/update-technology.dto';

export class CreateUserDto {
  @IsEmail()
  @ApiModelProperty()
  readonly email: string;

  @IsString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly first_name?: string;

  @IsString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly last_name?: string;

  @IsEnum(Gender)
  @ApiModelProperty({ enum: Gender })
  readonly gender: Gender;

  @IsNumber()
  @ApiModelProperty()
  readonly department: number;

  @IsNumber()
  @ApiModelProperty()
  readonly position: number;

  @IsNumber()
  @ApiModelProperty()
  readonly city: number;

  @IsNumber()
  @Min(1)
  @Max(10)
  @ApiModelProperty({ description: 'Value between 1 and 10' })
  readonly level: number;

  @IsArray()
  @ValidateNested()
  @Type(() => UpdateTechnologyDto)
  @ApiModelProperty()
  readonly technologies: UpdateTechnologyDto[];

  @IsString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly phone?: string;

  @IsString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly photo?: string;

  @IsNumber()
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly experience?: number;

  @IsString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly qualities?: string;

  @IsDateString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly date_start_job?: Date;

  @IsDateString()
  @IsOptional()
  @ApiModelProperty({ required: false })
  readonly date_end_job?: Date;

  @IsNumber()
  @ApiModelProperty()
  readonly role: number;

  @IsArray()
  @ValidateNested()
  @Type(() => CreateSocialDto)
  @ApiModelProperty()
  readonly social: CreateSocialDto[];

  @IsNumber()
  @ApiModelProperty()
  readonly english_level: number;
}
