import { ApiModelProperty } from '@nestjs/swagger';

export class RawUserDto {
  @ApiModelProperty({ example: 1 })
  readonly id: number;

  @ApiModelProperty({ example: 5 })
  readonly experience: number;

  @ApiModelProperty({ example: '2018-10-18T12:41:40.449Z' })
  readonly createdAt: string;

  @ApiModelProperty({ example: '2018-10-18T12:41:40.449Z' })
  readonly updatedAt: string;

  @ApiModelProperty({ example: 'mail@example.com' })
  readonly email: string;

  @ApiModelProperty({ example: 'Admin' })
  readonly first_name: string;

  @ApiModelProperty({ example: 'User' })
  readonly last_name: string;

  @ApiModelProperty({ example: '0 123 45 67 890' })
  readonly phone: string;

  @ApiModelProperty({ example: 'https://www.domain.com/my-image.png' })
  readonly photo: string;

  @ApiModelProperty({ example: 1 })
  readonly level: number;

  @ApiModelProperty({ example: 'MALE' })
  readonly gender: string;

  @ApiModelProperty({ example: '' })
  readonly qualities: string;

  @ApiModelProperty({ example: '' })
  readonly ssh_key: string;

  @ApiModelProperty({ example: '2018-10-18T12:41:40.449Z' })
  readonly date_start_job: string;

  @ApiModelProperty({ example: '2018-10-18T12:41:40.449Z' })
  readonly date_end_job: string;
}