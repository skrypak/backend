import { IsString, IsEmail } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class ResetUserPasswordCompleteDto {
  @IsString()
  @ApiModelProperty()
  readonly reset_token: string;

  @IsString()
  @ApiModelProperty()
  readonly password_hash: string;
}
