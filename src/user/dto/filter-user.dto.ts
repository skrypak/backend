import { IsString, IsOptional, IsEnum } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { QueryOrder } from 'common/enums/query-order.enum';

export class FilterUserDto {
    @IsString() @IsOptional() @ApiModelPropertyOptional() readonly position?: string;
    @IsString() @IsOptional() @ApiModelPropertyOptional() readonly city?: string;
    @IsString() @IsOptional() @ApiModelPropertyOptional() readonly department?: string;
    @IsString() @IsOptional() @ApiModelPropertyOptional() readonly technologies?: string;
    @IsString() @IsOptional() @ApiModelPropertyOptional() readonly search?: string;
    @IsString() @IsOptional() @ApiModelPropertyOptional() readonly page?: string;
    @IsString() @IsOptional() @ApiModelPropertyOptional() readonly count?: string;
    @IsString() @IsOptional() @ApiModelPropertyOptional() readonly orderBy?: string;
    @IsString() @IsOptional() @ApiModelPropertyOptional() readonly level?: string;
    @IsEnum(QueryOrder) @IsOptional() @ApiModelPropertyOptional() readonly orderType?: QueryOrder;
}
