import {
  Get,
  Controller,
  Post,
  UseGuards,
  Body,
  Param,
  Delete,
  Put,
  HttpCode,
  HttpException,
  HttpStatus,
  ClassSerializerInterceptor,
  UseInterceptors,
  Req,
  Query,
  FileInterceptor,
  UploadedFile,
  Headers,
} from '@nestjs/common';

import { UploadFileDto } from '../common/dto/uploadfile.dto';
import { FileLinkDto } from '../common/dto/filelink.dto';

import { AuthGuard } from '@nestjs/passport';
import { ApiUseTags, ApiBearerAuth, ApiOperation, ApiConsumes, ApiImplicitFile, ApiImplicitBody, ApiResponse } from '@nestjs/swagger';

import { User } from './user.entity';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { FilterUserDto } from './dto/filter-user.dto';
import { ResponseUserDto } from './dto/response-fetch-all-user.dto';
import { ResponseUserInfoDto } from './dto/response-user.dto';
import { Role } from 'common/enums/role.enum';
import { FileService } from '../core/file.service';
import { UserResetPasswordService } from 'user-reset-password/user-reset-password.service';

@ApiUseTags('user')
@Controller('user')
@UseInterceptors(ClassSerializerInterceptor)
export class UserController {

  constructor(
    private readonly userService: UserService,
    private readonly fileService: FileService) {
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ title: 'Get all users' })
  @ApiResponse({ status: 200, type: ResponseUserDto, isArray: true })
  async fetchAll(@Query() query: FilterUserDto): Promise<ResponseUserDto> {
    return this.userService.filterUsers(query);
  }

  @Get('/me')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ title: 'Get user by token' })
  @ApiResponse({ status: 200, type: ResponseUserInfoDto })
  async fetchUserByToken(@Req() req): Promise<User> {
    const { id } = req.user;
    return this.userService.findById(id);
  }

  @Get('/email')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ title: 'Get user by email' })
  @ApiResponse({ status: 200, type: ResponseUserInfoDto })
  async fetchUserByEmail(@Query('email') email: string): Promise<User> {
    return this.userService.findByEmail(email);
  }

  @Get('/all')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ title: 'Get users(not paginated)' })
  @ApiResponse({ status: 200, type: ResponseUserInfoDto, isArray: true })
  getAll(): Promise<User[]> {
    return this.userService.getAll();
  }

  @Get('/:id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ title: 'Get user by id' })
  @ApiResponse({ status: 200, type: ResponseUserInfoDto })
  async fetchUserById(@Param('id') userId: number): Promise<User> {
    const user = await this.userService.findById(userId);
    if (!user) throw new HttpException('Can\'t find user with this id', HttpStatus.BAD_REQUEST);
    return user;
  }

  @Get('/roles')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ title: 'Get roles' })
  @ApiResponse({ status: 200, description: 'Array of user roles' })
  fetchAllTypes(): string[] {
    return Object.keys(Role).map(e => Role[e as string]);
  }

  @Post('/photo')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @UseInterceptors(FileInterceptor('file'))
  @ApiOperation({ title: 'Upload user avatar' })
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'file', required: true, description: 'Image file for user profile avatar' })
  @ApiImplicitBody({ name: 'id', type: Number, required: true })
  @ApiResponse({ status: 200, description: 'Link to the uploaded file' })
  async uploadFile(@UploadedFile() file: UploadFileDto, @Body('id') userId: number): Promise<FileLinkDto> {
    if (!file.mimetype) throw new HttpException('Can\'t upload file with undefined format', HttpStatus.BAD_REQUEST);
    if (!userId) throw new HttpException('UserId is undefined!', HttpStatus.BAD_REQUEST);

    const user = await this.userService.findById(userId);
    if (!user) throw new HttpException('Can\'t find user with this id', HttpStatus.BAD_REQUEST);

    const filename = await this.fileService.uploadFile(file);
    if (!filename)  throw new HttpException('Can\'t upload file on S3', HttpStatus.BAD_REQUEST);

    return this.userService.updateUserPhoto({ userId, filename });
  }

  @Post()
  @ApiOperation({ title: 'Create / register a new user' })
  @ApiResponse({ status: 200, type: ResponseUserInfoDto })
  async create(@Body() userDto: CreateUserDto, @Headers('origin') origin: string): Promise<User> {
    const user = await this.userService.findByEmailOrPhone(userDto.email, userDto.phone);
    if (user) throw new HttpException('User with this email already registered', HttpStatus.BAD_REQUEST);
    return this.userService.create(userDto, origin);
  }

  @Put('/:id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ title: 'Update an existing user' })
  @ApiResponse({ status: 200, type: ResponseUserInfoDto })
  async update(@Body() userDto: UpdateUserDto, @Param('id') userId: number): Promise<User> {
    const user = await this.userService.findById(userId);
    if (!user) throw new HttpException('Can\'t find user with this id', HttpStatus.BAD_REQUEST);
    return this.userService.update(userDto);
  }

  @Delete('/:id')
  @HttpCode(204)
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOperation({ title: 'Delete user' })
  @ApiResponse({ status: 204, description: 'Successfully deleted user' })
  async remove(@Param('id') userId: number): Promise<any> {
    return this.userService.remove(userId);
  }
}
